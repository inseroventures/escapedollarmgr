﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web.Mvc;
using EscapeDollarMgr.Tools;
using ShopifySharp;
using System.Diagnostics;
using System.Globalization;
using EscapeDollarMgr.Data;
using EscapeDollarMgr.Models;
using ShopifySharp.Filters;
using Microsoft.Extensions.Primitives;
using Newtonsoft.Json;
using System.IO;
using System.Web.Script.Serialization;

namespace EscapeDollarMgr.Controllers
{
    public class HomeController : Controller
    {

        private EscCardManager _EscCardManager = new EscCardManager();
        private SendEmail _SendEmail = new SendEmail();
        private CultureInfo dtCulture = AppSettings.DateTimeCulture;
        private string secretKey = AppSettings.ShopifySecretKey;

        private bool IsPOSEmbedded (string ua)
        {
            return ua.Contains("com.jadedpixel") || ua.Contains("com.shopify");
        }

        public async Task<ActionResult> Index(string fName, string lName, string shop)
        {
            //check if logged in by seeing if session variables are set, otherwise check if redirect from Shopify (i.e. embedded) otherwise redirect to login
            // if already logged in then passing shop around in the query string. Have to pass around in the query string because IOS IPAD
            // needs it as after 12.4.1 upgrade session variables not holding value unless web.config sets cookieless=UseUri. Unfortunately shopify webhooks
            // wont work with this setting

            Trace.TraceInformation("Starting Index Method");
            string shopToken = "";
            if (!string.IsNullOrEmpty(shop))
            {
                shopToken = GetShopToken(shop);
            }
            //            string shop = (string)Session["Shop"];
            //             shopToken = (string)Session["ShopToken"];


            string rShop = Request.QueryString["shop"];

            //check if this is a request from Shopify and session shop matches shopify request otherwise set shop to null so go through authentication against request from Shopify
            if (!String.IsNullOrEmpty(rShop))
            {
                if (rShop != shop) shop = null; 
            }

            if (String.IsNullOrEmpty(shop))
            {
             
                //not logged in so check if its valid request from Shopify
                shop = Request.QueryString["shop"];
                Trace.TraceInformation("In Index shop is " + shop + " and " + Request["shop"]);
                bool isValidDomain = false;

               
                if (!String.IsNullOrEmpty(shop)) isValidDomain = await AuthorizationService.IsValidShopDomainAsync(shop);

                if (isValidDomain)
                {
                   
                    var queryString = Request.QueryString.Keys.Cast<string>().ToDictionary(k => k, v => (StringValues)Request.QueryString[v]);
                    if (AuthorizationService.IsAuthenticRequest(queryString, secretKey))
                    {
                        Trace.TraceInformation("Index Method - authentic request from Shopify");
                        //Request is authentic.
                        shopToken = GetShopToken(shop);
                        Session["Shop"] = shop;
                        Session["ShopToken"] = shopToken;

                    }
                    else
                    {
                        //redirect to shopify for user to install or login
                        return RedirectToAction(actionName: "Login", controllerName: "Fulfil");
                    }

                }
                else
                {
                    //redirect to shopify for user to install or login
                    return RedirectToAction(actionName: "Login", controllerName: "Fulfil");
                }

            }

            ViewBag.FName = fName;
            ViewBag.LName = lName;
           
            var service = new CustomerService(shop, shopToken);
            string qs = "";
            if (!String.IsNullOrEmpty(fName))
            {
                qs = qs + "first_name:'" + fName + "'";
            }

            if (!String.IsNullOrEmpty(lName))
            {
                qs = qs + " last_name:'" + lName + "'";
            }

            var listFields = new ShopifySharp.Filters.ListFilter()
            {
                Fields = "id,first_name,last_name,email,default_address,state"
                 
             };

            ViewBag.shop = shop;
            ViewBag.shopToken = shopToken;
            ViewBag.posEmbedded = IsPOSEmbedded(Request.UserAgent);

            IEnumerable<Customer> customers;
            if (String.IsNullOrEmpty(qs))
            {
                customers = await service.ListAsync(listFields);
            }
            else
            {
                customers = await service.SearchAsync(qs);
               
            }

            if (!String.IsNullOrEmpty(Request["Message"])) ViewBag.Message = Request["Message"];

            Trace.TraceInformation("Index Method - Presenting view");
            return View(customers);
        }

        public async Task<ActionResult> ShowCust(string custId, string shop, string mess)
        {

            Customer customer = new Customer();
            IEnumerable<MetaField> metafields;
            Trace.TraceInformation("Starting ShowCust with customer " + custId);
 //           string shop = (string)Session["Shop"];
 //           string shopToken = (string)Session["ShopToken"];
            string shopToken = GetShopToken(shop);
            ViewBag.shop = shop;

            var service = new CustomerService(shop, shopToken);
            try
            {
                customer = await service.GetAsync(Int64.Parse(custId));
            }
            catch (Exception e)
            {
                Trace.TraceError("In ShowCust - Error getting customer from Shopify " + custId + " " + e.ToString());
                return View("Error", "In ShowCust - Error getting customer from Shopify " + " " + custId + e.ToString());
            }

           
            ViewBag.customer = customer;
            var metaservice = new MetaFieldService(shop, shopToken);
            try
           {
                metafields = await metaservice.ListAsync((long)customer.Id, "customers");
            }
            catch (Exception e)
           {
               Trace.TraceError("In ShowCust - Error getting metafields from Shopify " + custId + " " + e.ToString());
                return View("Error", "In ShowCust - Error getting metafields from Shopify " + custId + " " + e.ToString());
           }

            var escCards = _EscCardManager.MetatoEscList(metafields);
            var escTotal = 0;
            var escExpired = 0;
            var escAvail = 0;
            DateTime nDate = DateTime.UtcNow;
            foreach (var esc in escCards)
            {
                escTotal = escTotal + Int32.Parse(esc.value);
                var eDate = DateTime.Parse(esc.expiry_date, dtCulture);
                if (eDate <= nDate)
                {
                    escExpired = escExpired + Int32.Parse(esc.value);
                }
                else
                {
                    escAvail = escAvail + Int32.Parse(esc.value);
                }


            };

            string otherLoyDol = _EscCardManager.OtherLoyDol(metafields);

            var names = new List<string> { "Not Set", "No", "Yes"};
            int ind = names.FindIndex(x => x == otherLoyDol);
            if (ind > -1)
            {
                names.RemoveAt(ind);
            }
            names.Insert(0, otherLoyDol);

            List<SelectListItem> xx = names.Select(f => new SelectListItem() { Value = f, Text = f }).ToList();
            

            ViewData["LoyDolOptions"] = xx;


            ViewBag.escTotal = (escTotal / 100).ToString("F2");
            ViewBag.escExpired = (escExpired / 100).ToString("F2");
            ViewBag.escAvail = (escAvail / 100).ToString("F2");

            ViewBag.escCards = escCards;

            ViewBag.mess = mess;
            ViewBag.posEmbedded = IsPOSEmbedded(Request.UserAgent);
            Trace.TraceInformation("ShowCust presenting view" + custId);
            return View();
        }

        public async Task<ActionResult> ShowCard(string custId, string cardId, string shop)
        {
            Customer customer = new Customer();
            IEnumerable<MetaField> metafields;
    //        string shop = (string)Session["Shop"];
   //         string shopToken = (string)Session["ShopToken"];
            string shopToken = GetShopToken(shop);
            ViewBag.shop = shop;

            Trace.TraceInformation("Starting ShowCard " + custId);
            var service = new CustomerService(shop, shopToken);

            try
            {
                customer = await service.GetAsync(Int64.Parse(custId));
            }
            catch (Exception e)
            {
                Trace.TraceError("In ShowCard - Error getting metafields from Shopify " + custId + " " + e.ToString());
                return View("Error", "In ShowCard - Error getting metafields from Shopify " + custId + " " + e.ToString());
            }

            ViewBag.customer = customer;
            var metaservice = new MetaFieldService(shop, shopToken);

            try
            {
                metafields = await metaservice.ListAsync((long)customer.Id, "customers");
            }
            catch (Exception e)
            {
                Trace.TraceError("In ShowCards - Error getting metafields from Shopify " + custId + " " + e.ToString());
                return View("Error", "In ShowCards - Error getting metafields from Shopify " + custId + " " + e.ToString());
            }

            var escCards = _EscCardManager.MetatoEscList(metafields);
     
            foreach (var card in escCards)
            {
                if (card.id == cardId)
                {
                    ViewBag.card = card;
                    break;
                }
            }

            ViewBag.posEmbedded = IsPOSEmbedded(Request.UserAgent);
            Trace.TraceInformation("ShowCard - presenting view - " + custId);
            return View();
        }

        public async Task<ActionResult> AddEscCard(string cardId, string title, string value, string date, string custId, string shop )
        {
            MetaField meta = new MetaField();
            Trace.TraceInformation("Starting AddEscCard - " + custId);

            DateTime checkDate;
            if (!DateTime.TryParse(date, dtCulture, DateTimeStyles.None, out checkDate))
            {
                ViewBag.ErrorMessage = "Invalid Date";            
            }
            else
            {

                //               string shop = (string)Session["Shop"];
                string shopToken = GetShopToken(shop);
                ViewBag.shop = shop;
//                string shopToken = (string)Session["ShopToken"];

                var metaservice = new MetaFieldService(shop, shopToken);
                meta = _EscCardManager.EscCardtoMeta(cardId, title, value, checkDate.ToString("dd-MM-yyyy"), "Giftcard raised for value of " + value);

                try
                {
                    meta = await metaservice.CreateAsync(meta, Int64.Parse(custId), "customers");
                }
                catch (Exception e)
                {
                    Trace.TraceError("In AddEscCard - Error creating metafield in Shopify " + custId + " " + e.ToString());
                    return View("Error", "In AddEscCard - Error creating metafield in Shopify " + custId + " " + e.ToString());
                }

            }

            Trace.TraceInformation("Leaving AddEscCard - " + custId);
            return RedirectToAction(actionName: "ShowCust", routeValues: new { cardId = cardId, custId = custId, shop=shop });
        }

        public async Task<ActionResult> UpdateCustLoyDol(string custId, string shop, string otherLoyDol)
        {
            IEnumerable<MetaField> metafields;
            Trace.TraceInformation("Starting Update Loyalty Dollars - " + custId);

            string shopToken = GetShopToken(shop);
            ViewBag.shop = shop;

            var metaservice = new MetaFieldService(shop, shopToken);

            try
            {
                metafields = await metaservice.ListAsync(Int64.Parse(custId), "customers");
            }
            catch (Exception e)
            {
                Trace.TraceError("In UpdateCustLoyDol - Error getting metafields from Shopify " + custId + " " + e.ToString());
                return View("Error", "In UpdateCustLoyDol - Error getting metafields from Shopify " + custId + " " + e.ToString());
            }

            var meta = new MetaField();

            foreach (var mMeta in metafields)
            {
                if (mMeta.Namespace == "LoyaltyOtherVendor")
                {
                    meta = mMeta;
                    break;
                }
            }

            if (otherLoyDol == "Not Set" && meta.Id != null)
            {
                try
                { 
                var metf = metaservice.DeleteAsync((long)meta.Id);
                }
                catch (Exception ex)
                {
                    Trace.TraceError("In UpdateCustLoyDol - Error deleting metafield  " + custId + " " + ex.Message);
                    return View("Error", "In UpdateCustLoyDol - Error deleting value " + custId + " " + ex.Message);
                }
            } 
            else
            {
                meta.Key = "LoyaltyOtherVendor";
                meta.Description = "Loyalty OtherVendors Dollars";
                meta.Namespace = "LoyaltyOtherVendor";
                meta.ValueType = "string";

                meta.Value = otherLoyDol;

                try
                {
                    var metaf = await metaservice.CreateAsync(meta, Int64.Parse(custId), "customers");
                }
                catch (Exception ex)
                {
                    Trace.TraceError("In UpdateCustLoyDol - Error updating metafield  " + custId + " " + ex.Message);
                    return View("Error", "In UpdateCustLoyDol - Error updating value " + custId + " " + ex.Message);
                }
            }

            Trace.TraceInformation("Leaving UpdateCustLoyDol - " + custId);
            return RedirectToAction(actionName: "ShowCust", routeValues: new { custId = custId, shop = shop });
        }

        public async Task<ActionResult> UpdateCard(string cardId, string Value, string expiryDate, string note, string custId, string shop)
        {
            IEnumerable<MetaField> metafields;

            Trace.TraceInformation("Starting UpdateCard - " + custId);

            DateTime CheckDate;
            if (!DateTime.TryParse(expiryDate, dtCulture, DateTimeStyles.None, out CheckDate))
            {
                ViewBag.ErrorMessage = "Invalid Date";

            }
            else
            {

 //               string shop = (string)Session["Shop"];
 //               string shopToken = (string)Session["ShopToken"];

                string shopToken = GetShopToken(shop);
                ViewBag.shop = shop;

                var metaservice = new MetaFieldService(shop, shopToken);

                try
                {
                    metafields = await metaservice.ListAsync(Int64.Parse(custId), "customers");
                }
                catch (Exception e)
                {
                    Trace.TraceError("In UpdateCard - Error getting metafields from Shopify " + custId + " " + e.ToString());
                    return View("Error", "In UpdateCard - Error getting metafields from Shopify " + custId + " " + e.ToString());
                }
                
                var meta = new MetaField();

                foreach (var mMeta in metafields)
                {
                    if (mMeta.Description.Contains(cardId))
                    {
                        meta = mMeta;
                        break;
                    }
                }

                if (meta != null)
                {
                    meta = _EscCardManager.UpdateMeta(meta, Value, expiryDate, note);
                }

                try
                {
                    meta = await metaservice.UpdateAsync((long)meta.Id, meta);
                }
                catch (Exception e)
                {
                    Trace.TraceError("In AddEscCard - Error updating metafield in Shopify " + custId + " " + e.ToString());
                    return View("Error", "In AddEscCard - Error updating metafield in Shopify " + custId + " " + e.ToString());
                }

            }
            Trace.TraceInformation("Leaving UpdateCard - " + custId);
            return RedirectToAction(actionName: "ShowCard", routeValues: new { cardId = cardId, custId = custId, shop=shop });
        }

        public async Task<ActionResult> DeleteCard(string cardId, string custId, string shop)
        {
            IEnumerable<MetaField> metafields;

            Trace.TraceInformation("Starting DeleteCard - " + custId);

            //            string shop = (string)Session["Shop"];
            //           string shopToken = (string)Session["ShopToken"];

            string shopToken = GetShopToken(shop);
            ViewBag.shop = shop;

            var metaservice = new MetaFieldService(shop, shopToken);
            try
            {
                metafields = await metaservice.ListAsync(Int64.Parse(custId), "customers");
            }
            catch (Exception e)
            {
                Trace.TraceError("In DeleteCard - Error getting metafields from Shopify " + custId + " " + e.ToString());
                return View("Error", "In DeleteCard - Error getting metafields from Shopify " + custId + " " + e.ToString());
            }


            foreach (var meta in metafields)
            {
                if (meta.Description.Contains(cardId))
                {
                    try
                    {
                        await metaservice.DeleteAsync((long)meta.Id);
                        break;
                    }
                    catch (Exception e)
                    {
                        Trace.TraceError("In DeleteCard - Error deleting metafield in Shopify " + custId + " " + meta.Id + " " + e.ToString());
                        return View("Error", "In DeleteCard - Error deleting metafield in Shopify " + custId + " " + meta.Id + " " + e.ToString());
                    }
                }
            }

            Trace.TraceInformation("Leaving DeleteCard - " + custId);
            return RedirectToAction(actionName: "ShowCust", routeValues: new {custId = custId, shop=shop });
        }

        public async Task<ActionResult>SendAccountActivation(string custId)
        {
            Customer customer = new Customer();
            string url = "";

            Trace.TraceInformation("Starting SendAccountActivation with customer " + custId);
            string shop = (string)Session["Shop"];
            string shopToken = (string)Session["ShopToken"];

            var service = new CustomerService(shop, shopToken);
            try
            {
                customer = await service.GetAsync(Int64.Parse(custId));
            }
            catch (Exception e)
            {
                Trace.TraceError("In SendAccountActivation - Error getting metafields from Shopify " + custId + " " + e.ToString());
                return View("Error", "In SendAccountActivation - Error getting metafields from Shopify " + custId + " " + e.ToString());
            }

            if (customer.State == "enabled")
            {
                return RedirectToAction(actionName: "ShowCust", routeValues: new { custId = custId, Mess = "Already enabled - no activation Url to send" });
            }
            else
            {

                try
                {
                    url = await service.GetAccountActivationUrl((long)customer.Id);
                }
                catch (Exception e)
                {
                    Trace.TraceError("In SendAccountActivation - Error getting activation url for " + customer.Email + " " + e.ToString());
                    return View("Error", "In SendAccountActivation - Error getting Error getting activation url for " + customer.Email + " " + e.ToString());
                }

                Trace.TraceInformation("SendAccountActivation - Submitting Sendgrid Activation email - " + custId);
                _SendEmail.submitactivationemail(customer.FirstName, customer.Email, url);

                Trace.TraceInformation("Leaving SendAccountActivation - " + custId);
                return RedirectToAction(actionName: "ShowCust", routeValues: new { custId = custId, Mess = "Submitted Email", shop=shop });
            }
        }

        private string GetShopToken(string shop)
        {
            using (var con = new EscapeDollarMgrEntities())
            {
                ShopData x = con.ShopDatas.First(c => c.Shop == shop);
                return x.Token;
            }
        }

        public async Task<ActionResult> Giftcard()
        {
            Trace.TraceInformation("Starting Giftcard");
            // need to check if authentic Shopify request
            var queryString = Request.QueryString.Keys.Cast<string>().ToDictionary(k => k, v => (StringValues)Request.QueryString[v]);
            if (AuthorizationService.IsAuthenticRequest(queryString, secretKey))
            {
                //Request is authentic.
                Trace.TraceInformation("Giftcard - confirmed authentic");
                string shop = Request.QueryString["shop"];
                string shopToken = GetShopToken(shop);

                Trace.TraceInformation("Giftcard with shop " + shop);


        //        Session["Shop"] = shop;
        //        Session["ShopToken"] = shopToken;
                ViewBag.shop = shop;
                ViewBag.shopToken = shopToken;
                ViewBag.GiftCard = "Y"; // so _viewstart doesnt apply a layout.
                
                if (Request.UserAgent.Contains("com.jadedpixel") || Request.UserAgent.Contains("com.shopify")) ViewBag.posEmbedded = true;
                
                Trace.TraceInformation("Giftcard presenting view");
                return View();
            }
            else
            {
                Trace.TraceError("In GiftCard - Not a valid ShopifyRequest ");
                return View("Error", "In GiftCard - Not a valid ShopifyRequest");
            }
        }

        [HttpGet]
        public async Task<ActionResult>CheckCards(string id, string orderTotal, string shop, string token, string pIds )
        {
            Customer customer = new Customer();
            IEnumerable<MetaField> metafields;
            IEnumerable<Product> products;

            Trace.TraceInformation("Starting CheckCards, cust id is " + id);

   //         string shop = (string)Session["Shop"]; //this was set by giftcards above
    //        string shopToken = (string)Session["ShopToken"];

    //        Trace.TraceInformation("CheckCards, have session variables. Shop is " + shop);

            var custService = new CustomerService(shop, token);

            try
            {
                customer = await custService.GetAsync(Int64.Parse(id));
            }
            catch (Exception e)
            {
                Trace.TraceError("In CheckCards - Error getting customer from Shopify " + id + " " + e.ToString());
                return View("Error", "In CheckCards - Error getting customer from Shopify " + " " + id + e.ToString());
            }

            var metaService = new MetaFieldService(shop, token);
            try
            {
                  metafields = await metaService.ListAsync(Int64.Parse(id), "customers");
            }
            catch (Exception e)
            {
                   Trace.TraceError("In CheckCards - Error getting metafields from Shopify " + id + " " + e.ToString());
                   return View("Error", "In CheckCards - Error getting metafields from Shopify " + id + " " + e.ToString());
              }

            var escCards = _EscCardManager.MetatoEscList(metafields, noexpired: true, gtrthan0: true);

            Double cardValue = 0;
            foreach (var card in escCards)
            {
                cardValue = cardValue + Int64.Parse(card.value);
            }

            // check the products listed and see which ones have esc$ applicable, i.e. is there a tag na_E$

            string escAppl = "";

            var prodService = new ProductService(shop, token);

            var pArray = pIds.Split(',');
            
            foreach (var productId in pArray)
            {
                if (String.IsNullOrEmpty(productId)) continue;

                var product = await prodService.GetAsync(Convert.ToInt64(productId));

                if (product != null && !product.Tags.Contains("na_E$"))
                {
                    escAppl = escAppl + productId + ",";
                }
            }

            ViewBag.otherLoyDol = _EscCardManager.OtherLoyDol(metafields);

            ViewBag.cardValue = cardValue / 100;
            ViewBag.orderTotal = float.Parse(orderTotal).ToString("0.00");
            ViewBag.escAppl = escAppl;
            ViewBag.firstName = customer.FirstName;
            ViewBag.escCards = escCards;
            ViewBag.shop = shop;
            ViewBag.shopToken = token;
            ViewBag.posEmbedded = IsPOSEmbedded(Request.UserAgent);

            Trace.TraceInformation("CheckCards, presenting view - cust id is " + id);
            return View();
        }

        public async Task<ActionResult> UpdMinDealerDollar(string minOrderValueA, string minOrderValueB, string minOrderValueC)
        {
            Trace.TraceInformation("Starting UpdMinDealerDollar");

            string shop = (string)Session["Shop"];
            string shopToken = (string)Session["ShopToken"];

            IEnumerable<MetaField> mfields;
            var metaservice = new MetaFieldService(shop, shopToken);
            MetaFieldFilter mFilter = new MetaFieldFilter();
            mFilter.Key = "Dealer Min Order Value";

            try
            {
                mfields = await metaservice.ListAsync(mFilter);
            }
            catch (Exception e)
            {
                Trace.TraceError("In UpdMinDealerDollar - Error getting dealer min order value metafield from Shopify " + e.ToString());
                return View("Error", "In UpdMinDealerDollar - Error getting dealer min order value metafield from Shopify " + e.ToString());
            }

            String mValue = "{\"A\":\"" + minOrderValueA + "\",\"B\":\"" + minOrderValueB + "\",\"C\":\"" + minOrderValueC + "\"}";

            var meta = new MetaField();

            if (mfields != null && mfields.Any())
            {
                // ok found one 
                meta = mfields.First();
                meta.Value = mValue;

                try
                {
                    meta = await metaservice.UpdateAsync((long)meta.Id, meta);
                }
                catch (Exception e)
                {
                    Trace.TraceError("In UpdMinDealerDollar - Error updating dealer min order value metafield in Shopify " + e.ToString());
                    return View("Error", "In UpdMinDealerDollar - Error updating dealer min order value metafield in Shopify " + e.ToString());
                }
            }
            else
            {
                // ok doesnt exist so create
                meta.Key = "Dealer Min Order Value";
                meta.Description = "Dealer Min Order Value";
                meta.Namespace = "Dealer_Rules";
                meta.Value = mValue;
                meta.ValueType = "string";

                try
                {
                    meta = await metaservice.CreateAsync(meta);
                }
                catch (Exception e)
                {
                    Trace.TraceError("In UpdMinDealerDollar - Error creating dealer min order value metafield in Shopify " + e.ToString());
                    return View("Error", "In UpdMinDealerDollar - Error creating dealer min order value metafield in Shopify " + e.ToString());
                }
            }
           
            return RedirectToAction(actionName: "DealerRules");
        }

        public async Task<ActionResult> ViewMoqRule(string Id)
        {
            Trace.TraceInformation("Starting ViewMoqRule");

            string shop = (string)Session["Shop"];
            string shopToken = (string)Session["ShopToken"];

            var meta = new MetaField();
            var metaservice = new MetaFieldService(shop, shopToken);

            IEnumerable<MetaField> mfields;
            MetaFieldFilter mFilter = new MetaFieldFilter();
            mFilter.Namespace = "Dealer_Rules";
            try
            {
                mfields = await metaservice.ListAsync(mFilter);
            }
            catch (Exception e)
            {
                Trace.TraceError("In ViewMoqRules - Error getting metafield Dealer Rules from Shopify " + e.ToString());
                return View("Error", "In ViewMoqRules - Error getting metafield Dealer Rules from Shopify " + e.ToString());
            }

            foreach (MetaField mfield in mfields)
            {
                if ( mfield.Value.ToString().Contains(Id))
                {
                    meta = mfield;
                    break;
                }
            }

            ViewBag.MoqRule = JsonConvert.DeserializeObject<MoqRuleObj>(meta.Value.ToString());

            return View();
        }

        public async Task<ActionResult> UpdMoqRule(string Id, string productTypes, string dealerAMinMoq, string dealerBMinMoq, string dealerCMinMoq)
        {
            Trace.TraceInformation("Starting UpdMoqRule");

            string shop = (string)Session["Shop"];
            string shopToken = (string)Session["ShopToken"];

            var meta = new MetaField();
            var metaservice = new MetaFieldService(shop, shopToken);

            IEnumerable<MetaField> mfields;
            MetaFieldFilter mFilter = new MetaFieldFilter();
            mFilter.Namespace = "Dealer_Rules";
            try
            {
                mfields = await metaservice.ListAsync(mFilter);
            }
            catch (Exception e)
            {
                Trace.TraceError("In ViewMoqRules - Error getting metafield Dealer Rules from Shopify " + e.ToString());
                return View("Error", "In ViewMoqRules - Error getting metafield Dealer Rules from Shopify " + e.ToString());
            }

            foreach (MetaField mfield in mfields)
            {
                if (mfield.Value.ToString().Contains(Id))
                {
                    meta = mfield;
                    break;
                }
            }

            if (meta.Id == null) return RedirectToAction(actionName: "DealerRules");

            String mValue = "{\"Id\":\"" + Id + "\",\"ProductTypes\":\"" + productTypes + "\",\"A\":\"" + dealerAMinMoq +
               "\",\"B\":\"" + dealerBMinMoq + "\",\"C\":\"" + dealerCMinMoq + "\"}";

            meta.Value = mValue;


            try
            {
                meta = await metaservice.UpdateAsync((long)meta.Id, meta);
            }
            catch (Exception e)
            {
                Trace.TraceError("In UpdMoqRule - Error updating moq rule metafield in Shopify " + e.ToString());
                return View("Error", "In UpdMoqRule - Error updating moq rule metafield in Shopify " + e.ToString());
            }

            return RedirectToAction(actionName: "DealerRules");

        }

        public async Task<ActionResult> DelMoqRule(string Id)
        {
            Trace.TraceInformation("Starting DelMoqRule");

            string shop = (string)Session["Shop"];
            string shopToken = (string)Session["ShopToken"];

            var meta = new MetaField();
            var metaservice = new MetaFieldService(shop, shopToken);

            IEnumerable<MetaField> mfields;
            MetaFieldFilter mFilter = new MetaFieldFilter();
            mFilter.Namespace = "Dealer_Rules";
            try
            {
                mfields = await metaservice.ListAsync(mFilter);
            }
            catch (Exception e)
            {
                Trace.TraceError("In ViewMoqRules - Error getting metafield Dealer Rules from Shopify " + e.ToString());
                return View("Error", "In ViewMoqRules - Error getting metafield Dealer Rules from Shopify " + e.ToString());
            }

            foreach (MetaField mfield in mfields)
            {
                if (mfield.Value.ToString().Contains(Id))
                {
                    meta = mfield;
                    break;
                }
            }

            if (meta != null)
            {
                // ok we have it
                try
                {
                    await metaservice.DeleteAsync((long)meta.Id);
                }
                catch (Exception e)
                {
                    Trace.TraceError("In UpdMoqRule - Error deleting moq rule metafield in Shopify " + e.ToString());
                    return View("Error", "In UpdMoqRule - Error deleting moq rule metafield in Shopify " + e.ToString());
                }

            }

            return RedirectToAction(actionName: "DealerRules");
        }

        public async Task<ActionResult> AddMoqRule(string prodTypes, string dealerAMinMoq, string dealerBMinMoq, string dealerCMinMoq)
        {
            Trace.TraceInformation("Starting AddMoqRule");

            string shop = (string)Session["Shop"];
            string shopToken = (string)Session["ShopToken"];

            var meta = new MetaField();
            var metaservice = new MetaFieldService(shop, shopToken);

            // get a more or less unique number based on secs since 1 Jan 1970
            DateTime origin = new DateTime(1970, 1, 1, 0, 0, 0, 0, DateTimeKind.Utc);
            TimeSpan diff = DateTime.UtcNow.ToUniversalTime() - origin;
            double guid = Math.Floor(diff.TotalSeconds);

            String mValue = "{\"Id\":\"" + guid.ToString() + "\",\"ProductTypes\":\"" + prodTypes + "\",\"A\":\"" + dealerAMinMoq +
                "\",\"B\":\"" + dealerBMinMoq + "\",\"C\":\"" + dealerCMinMoq + "\"}";

            meta.Key = "DMOQRules" + guid.ToString();
            meta.Description = "Dealer MOQ Rules";
            meta.Namespace = "Dealer_Rules";
            meta.Value = mValue;
            meta.ValueType = "string";

            try
            {
                meta = await metaservice.CreateAsync(meta);
            }
            catch (Exception e)
            {
                Trace.TraceError("In  AddMoqRule - Error adding metafield in Shopify " + e.ToString());
                return View("Error", "In  AddMoqRule - Error adding metafield in Shopify " + e.ToString());
            }

            return RedirectToAction(actionName: "DealerRules");
        }


        public async Task<ActionResult> DealerRules(string pType)
        {
            IEnumerable<MetaField> metafields;
            Trace.TraceInformation("Starting DealerRules");

            string shop = (string)Session["Shop"];
            string shopToken = (string)Session["ShopToken"];

            var metaservice = new MetaFieldService(shop, shopToken);
            MetaFieldFilter mFilter = new MetaFieldFilter();
            mFilter.Key = "Dealer Min Order Value";

            try
            {
                metafields = await metaservice.ListAsync(mFilter);
            }
            catch (Exception e)
            {
                Trace.TraceError("In DealerRules - Error getting metafields from Shopify " + e.ToString());
                return View("Error", "In DealerRules - Error getting metafields from Shopify " + e.ToString());
            }

            if (metafields != null && metafields.Any())
            {
                @ViewBag.MinOrderValue = JsonConvert.DeserializeObject<MinOrderValueObj>(metafields.First().Value.ToString());
            }
            else
            {
                MinOrderValueObj m = new MinOrderValueObj();
                m.A = "NF";
                @ViewBag.MinOrderValue = m;
            }

            mFilter = new MetaFieldFilter();
            metaservice = new MetaFieldService(shop, shopToken);
            mFilter.Namespace = "Dealer_Rules";
            try
            {
                metafields = await metaservice.ListAsync(mFilter);
            }
            catch (Exception e)
            {
                Trace.TraceError("In DealerRules - Error getting metafield Dealer Rules from Shopify " + e.ToString());
                return View("Error", "In DealerRules - Error getting metafield Dealer Rules from Shopify " + e.ToString());
            }

            List<MoqRuleObj> MoqRules = new List<MoqRuleObj>();
            if (metafields != null && metafields.Any())
            {
                foreach (var meta in metafields)
                {
                    if (meta.Description == "Dealer MOQ Rules")
                    {
                        MoqRuleObj mRule = JsonConvert.DeserializeObject<MoqRuleObj>(meta.Value.ToString());

                        if (String.IsNullOrEmpty(pType))
                        {
                            MoqRules.Add(mRule);
                        }
                        else
                        {
                            if (meta.Value.ToString().Contains(pType))
                            {
                                MoqRules.Add(mRule);
                            }

                        }
                    }
                    
                }
            }

            ViewBag.MoqRules = MoqRules;
            ViewBag.pType = pType;


            return View();
        }

        public async Task<ContentResult> Shipping ()
        {
            Trace.TraceInformation("Starting Shipping");
            var requestHeaders = Request.Headers.Keys.Cast<string>().ToDictionary(k => k, v => (StringValues)Request.Headers[v]);
            Stream inputStream = Request.InputStream;

            if (!await AuthorizationService.IsAuthenticWebhook(requestHeaders, Request.InputStream, secretKey))
            {
                throw new UnauthorizedAccessException("Shipping - This request is not an authentic Shopifty shipping request.");
            }

            Trace.TraceInformation("Shippnig URL valid - Getting Shop");
            string shop = (string)requestHeaders["X-Shopify-Shop-Domain"];
            string shopToken = GetShopToken(shop);

            Request.InputStream.Position = 0;
            string data = new StreamReader(Request.InputStream).ReadToEnd().ToString().Trim();

            if (data == "")
            {
                Trace.TraceError("In Shipping - No data inputstream");
                return Content("", "application/json");
            }

            //do something with data
            var jsonSerializer = new JavaScriptSerializer();
            var shipRequest = JsonConvert.DeserializeObject<ShipRateRequest>(data);

            if (String.IsNullOrEmpty(shipRequest.rate.Destination.CompanyName))
            {
                Trace.TraceError("In Shipping - Company Name null or empty");
                return Content("", "application/json");
            }

            IEnumerable<MetaField> metafields;
            var mFilter = new MetaFieldFilter();
            var metaservice = new MetaFieldService(shop, shopToken);
            mFilter.Namespace = "DealersList";
            mFilter.Limit = 250;

            try
            {
                metafields = await metaservice.ListAsync(mFilter);
            }
            catch (Exception e)
            {
                Trace.TraceError("In ViewDealer - Error getting DealerList metafields from Shopify " + e.ToString());
                return Content("", "application/json");
            }

            var foundDealer = false;
            foreach (var meta in metafields) {
     //           Trace.TraceInformation("Shipping - " + meta.Description);
                if (meta.Description.ToLower().Contains(shipRequest.rate.Destination.CompanyName.ToLower()))
                {
                    var dealer = JsonConvert.DeserializeObject<DealerItem>(meta.Value.ToString());
    //                Trace.TraceInformation("Shipping - " + dealer.Address1 + " " + dealer.PostCode);
                    if (dealer.Address1 == shipRequest.rate.Destination.Address1 && dealer.PostCode == shipRequest.rate.Destination.PostalCode)
                    {
                        foundDealer = true;
                        break;
                    }
                }
            };

            if (foundDealer)
            {
                ShipRateResponseItem rateItem = new ShipRateResponseItem
                {
                    ServiceName = "Store/Dealer Pickup Free Shipping",
                    ServiceCode = "2EFree",
                    TotalPrice = "0",
                    Currency = "RM",
                    MinDeliveryDate = "",
                    MaxDeliveryDate = ""
                };

                List<ShipRateResponseItem> rate = new List<ShipRateResponseItem>();
                rate.Add(rateItem);

                ShipRateResponse rateResponse = new ShipRateResponse();
                rateResponse.Rates = rate;

                string rateStr = JsonConvert.SerializeObject(rateResponse);
                var x = Json(rateResponse);
                return Content(rateStr, "application/json");
            }
            else
            {
                return Content("", "application/json"); 
            }

        }

        public async Task<ActionResult> UpdDealer()
        {
            IEnumerable<MetaField> metafields;
            Trace.TraceInformation("Update Dealer");

            string actionType = Request["actionType"];

            //           string shop = (string)Session["Shop"];
            //           string shopToken = (string)Session["ShopToken"];
            string shop = Request["shop"];
            string shopToken = GetShopToken(shop);
            ViewBag.shop = shop;


            var mFilter = new MetaFieldFilter();
            var metaservice = new MetaFieldService(shop, shopToken);
            mFilter.Namespace = "DealersList";
            mFilter.Limit = 250;
            try
            {
                metafields = await metaservice.ListAsync(mFilter);
            }
            catch (Exception e)
            {
                Trace.TraceError("In UpdDealer - Error getting DealerList metafields from Shopify " + e.ToString());
                return RedirectToAction("ManageDealers", "Home", new { shop = shop, Message = "In UpdDealer - Error getting DealerList metafields from Shopify " + e.ToString() });
            }

            DealerItem dealerItem = new DealerItem();
            bool foundOne = false;
            MetaField fMeta = new MetaField();
            foreach (var meta in metafields)
            {
                if (meta.Description.Contains(Request["dealer"]))
                {

                    foundOne = true;
                    fMeta = meta;
                    break;

                }
            }

            if (actionType == "add" && foundOne)
            {
                Trace.TraceError("In UpdDealer - Attempted to add a duplicate dealer ");
                return RedirectToAction("ManageDealers", "Home", new { shop = shop, Message = "In UpdDealer - Dealer not added as a dealer of that name already exists" });
            }

            DealerItem dealer = new DealerItem();

            dealer.Dealer = Request["dealer"].Trim();
            dealer.Contact = Request["contact"].Trim();
            dealer.Phone = Request["phone"].Trim();
            dealer.Address1 = Request["address1"].Trim();
            dealer.Address2 = Request["address2"].Trim();
            dealer.City = Request["city"].Trim();
            dealer.State = Request["state"].Trim();
            dealer.Country = Request["country"].Trim();
            dealer.PostCode = Request["postcode"].Trim();
            dealer.DealerType = Request["dealertype"].Trim();

            dealer.Key = Request["key"];
            if (string.IsNullOrEmpty(dealer.Key)) // happens if it is an add and being set for the first time
            {
                dealer.Key = dealer.Dealer.Replace(" ", "").Replace("&", "").ToLower();
            };


            dealer.Normal = Request["normal"] == "on";
            dealer.Triathlon = Request["triathlon"] == "on";
            dealer.HiRoad = Request["hiroad"] == "on";
            dealer.HiMTB = Request["himtb"] == "on";

            List<DealerOpeningTimes> openingTimes = new List<DealerOpeningTimes>();

            for (var i = 0; i <= 6; i++)
            {
                DealerOpeningTimes open = new DealerOpeningTimes();
                open.Day = i;
                open.Open = Request["open" + i.ToString()];
                open.Close = Request["close" + i.ToString()];
                open.NotOpen = Request["disabled" + i.ToString()] == "on";
                openingTimes.Add(open);
            }

            dealer.OpeningTimes = openingTimes;
            var aDealer = JsonConvert.SerializeObject(dealer);

            if (!foundOne)
            {
                // add it
                fMeta.Key = "Dealer-" + DateTime.UtcNow.ToString("yyyyMMddHHmmss");
                fMeta.Description = "Dealer-" + dealer.Dealer;
                fMeta.Namespace = "DealersList";
                fMeta.Value = JsonConvert.SerializeObject(dealer);
                fMeta.ValueType = "string";

                Trace.TraceInformation("Update Dealer - adding " + dealer.Dealer);

                try
                {
                    fMeta = await metaservice.CreateAsync(fMeta);
                }
                catch (Exception e)
                {
                    Trace.TraceError("In UpdDealer - Error adding " + dealer.Dealer + " metafield in Shopify " + e.ToString());
                    return RedirectToAction("ManageDealers", "Home", new { shop = shop, Message = "In UpdDealer - Error adding " + dealer.Dealer + "metafield in Shopify " + e.ToString() });
                }

                Trace.TraceInformation("Update Dealer - added " + dealer.Dealer);

            }
            else
            {
                // update it

                fMeta.Value = JsonConvert.SerializeObject(dealer);
                //   fMeta.Key = "Dealer-" + DateTime.UtcNow.ToString("yyyyMMddHHmmss");   
                fMeta.Description = "Dealer-" + dealer.Dealer;



                try
                {
                    fMeta = await metaservice.UpdateAsync((long)fMeta.Id, fMeta);
                }
                catch (Exception e)
                {
                    Trace.TraceError("In UpDealer - Error updating " + dealer.Dealer + " in Shopify " + e.ToString());
                    return RedirectToAction("ManageDealers", "Home", new { shop = shop, Message = "In UpDealer - Error updating " + dealer.Dealer + " metafield in Shopify " + e.ToString() });
                }

                Trace.TraceInformation("Update Dealer - updating " + dealer.Dealer);



            }

            return RedirectToAction(actionName: "ManageDealers", routeValues: new { shop = shop });
        }


        public async Task<ActionResult> AddDealer(string name, string shop)
        {
            DealerItem dealer = new DealerItem();

            DealerOpeningTimes open = new DealerOpeningTimes();
            List<DealerOpeningTimes> openingTimes = new List<DealerOpeningTimes>();

            dealer.Normal = true;
            dealer.Triathlon = true;
            dealer.HiRoad = true;
            dealer.HiMTB = true;
            dealer.DealerType = "A";

            for (var i = 0; i <= 6; i++)
            {
                open.Day = i;
                open.Open = "08:00 AM";
                open.Close = "08:00 PM";
                open.NotOpen = false;
                openingTimes.Add(open);
            }

            dealer.OpeningTimes = openingTimes;

            ViewBag.dealerItem = dealer;
            ViewBag.shop = shop;
            return View();

        }



        public async Task<ActionResult> ViewDealer(string name, string shop)
        {
            IEnumerable<MetaField> metafields;
            Trace.TraceInformation("ViewDealer");

 //           string shop = (string)Session["Shop"];
 //           string shopToken = (string)Session["ShopToken"];

            string shopToken = GetShopToken(shop);
            ViewBag.shop = shop;


            var mFilter = new MetaFieldFilter();
            var metaservice = new MetaFieldService(shop, shopToken);
            mFilter.Namespace = "DealersList";
            mFilter.Limit = 250;
            try
            {
                metafields = await metaservice.ListAsync(mFilter);
            }
            catch (Exception e)
            {
                Trace.TraceError("In ViewDealer - Error getting DealerList metafields from Shopify " + e.ToString());
                return RedirectToAction("ManageDealers", "Home", new { shop = shop, Message = "In ViewDealer - Error getting DealerList metafields from Shopify " + e.ToString() } );
            }

            DealerItem dealerItem = new DealerItem();
            foreach (var meta in metafields)
            {
                if (meta.Description.Contains(name))
                {

                    dealerItem = JsonConvert.DeserializeObject<DealerItem>(meta.Value.ToString());

                    if (String.IsNullOrEmpty(dealerItem.Key))
                    {
                        string key = dealerItem.Dealer.Replace(" ", "");
                        key = key.Replace("&", "");

                        dealerItem.Key = key.ToLower();
                    }


                    break;

                }
            }

            ViewBag.dealerItem = dealerItem;
            ViewBag.Id = dealerItem.Dealer;
            return View();

        }

        public async Task<ActionResult> DeleteDealer(string name, string shop)
        {

            Trace.TraceInformation("Starting DeleteDealer");

            //            string shop = (string)Session["Shop"];
            //            string shopToken = (string)Session["ShopToken"];

            string shopToken = GetShopToken(shop);
            ViewBag.shop = shop;


            var meta = new MetaField();
            var metaservice = new MetaFieldService(shop, shopToken);

            IEnumerable<MetaField> mfields;
            MetaFieldFilter mFilter = new MetaFieldFilter();
            mFilter.Namespace = "DealersList";
            mFilter.Limit = 250;
            try
            {
                mfields = await metaservice.ListAsync(mFilter);
            }
            catch (Exception e)
            {
                Trace.TraceError("In DeleteDealer - Error retrieving dealer metafields " + e.ToString());
                return RedirectToAction("ManageDealers", "Home", new { shop = shop, Message = "In DeleteDealer - Error retrieving dealer metafields " + e.ToString() } );
            }

            foreach (MetaField mfield in mfields)
            {
                if (String.IsNullOrEmpty(name))
                {
                    if (mfield.Description == "Dealer-")
                    {
                        meta = mfield;
                        break;
                    }
                }

                if (mfield.Description.Contains(name))
                {
                    meta = mfield;
                    break;
                }
            }

            if (meta != null)
            {
                // ok we have it
                try
                {
                    await metaservice.DeleteAsync((long)meta.Id);
                }
                catch (Exception e)
                {
                    Trace.TraceError("In DeleteDealer - Error deleting dealer metafield in Shopify " + e.ToString());
                    return RedirectToAction("ManageDealers", "Home", new { shop = shop, Message = "In DeleteDealer - Error deleting dealer metafield in Shopify " + e.ToString() } );
                }

            }

            return RedirectToAction(actionName: "ManageDealers", routeValues: new { shop = shop });

        }

        public async Task<ActionResult> ManageDealers(string dFilter, string shop)
        {
            IEnumerable<MetaField> metafields;
            Trace.TraceInformation("ManageDealers");

            //            string shop = (string)Session["Shop"];
            //            string shopToken = (string)Session["ShopToken"];

            string shopToken = GetShopToken(shop);
            ViewBag.shop = shop;


            var mFilter = new MetaFieldFilter();
            var metaservice = new MetaFieldService(shop, shopToken);
            mFilter.Namespace = "DealersList";
            mFilter.Limit = 250;
            try
            {
                metafields = await metaservice.ListAsync(mFilter);
            }
            catch (Exception e)
            {
                Trace.TraceError("In ManageDealers - Error getting DealerList metafields from Shopify " + e.ToString());
                return RedirectToAction("Index", "Home",  new { Message = "In ManageDealers - Error getting DealerList metafields from Shopify " + e.ToString() });
            }


            List<DealerItem> dealerList = new List<DealerItem>();
            if (metafields != null && metafields.Any())
            {
                foreach (var meta in metafields)
                {
                 //   if (meta.Description.Contains("Dealer"))
                 //   {

                    DealerItem dealerItem = JsonConvert.DeserializeObject<DealerItem>(meta.Value.ToString());

                    if (String.IsNullOrEmpty(dealerItem.Key))
                    {
                        string key = dealerItem.Dealer.Replace(" ", "");
                        key = key.Replace("&", "");

                        dealerItem.Key = key.ToLower();
                    }

                        if (String.IsNullOrEmpty(dFilter))
                        {
                            dealerList.Add(dealerItem);
                        }
                        else
                        {
                            if (meta.Value.ToString().ToLower().Contains(dFilter.ToLower()))
                            {
                                dealerList.Add(dealerItem);
                            }

                        }
                   // }

                }
            }

            dealerList = dealerList.OrderBy(p => p.Dealer).ToList();

            ViewBag.dealerList = dealerList;
            ViewBag.dFilter = dFilter;

            if (!String.IsNullOrEmpty(Request["Message"])) ViewBag.Message = Request["Message"];


            return View();

        }

            public ActionResult About()
        {
            ViewBag.Message = "Your application description page.";

            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }

        public ActionResult Error(string Mess)
        {
            ViewBag.Message = Mess;
            return View();
        }

    }
}