﻿using System;
using System.Collections.Specialized;
using System.Collections.Generic;
using System.IO;
using System.Web.Mvc;
using System.Diagnostics;
using System.Threading.Tasks;
using ShopifySharp;
using EscapeDollarMgr.Tools;
using EscapeDollarMgr.Data;
using System.Web.Script.Serialization;
using Microsoft.Extensions.Primitives;
using System.Linq;
using Newtonsoft.Json;
using System.Text.RegularExpressions;
using EscapeDollarMgr.Models;

namespace EscapeDollarMgr.Controllers
{
    public class WebHooksController : Controller
    {
        private string secretKey = AppSettings.ShopifySecretKey;
        private int expiryPeriod = Int32.Parse(AppSettings.ExpiryPeriod);
        private EscCardManager _EscCardManager = new EscCardManager();
        private SendEmail _SendEmail = new SendEmail();
        private string Ambassador = AppSettings.Ambassador;


        private string GetShopToken(string shop)
        {
            using (var con = new EscapeDollarMgrEntities())
            {
              ShopData x = con.ShopDatas.First(c => c.Shop == shop);
                return x.Token;  
            }
        }

        // required for GDPR compliance - set from the Shopify partner app config menu
        public async Task<string> CustErase()
        
        {
            Trace.TraceInformation("Starting CustErase webhook ");
            var requestHeaders = Request.Headers.Keys.Cast<string>().ToDictionary(k => k, v => (StringValues)Request.Headers[v]);
            Stream inputStream = Request.InputStream;

            if (!await AuthorizationService.IsAuthenticWebhook(requestHeaders, Request.InputStream, secretKey))
            {
                throw new UnauthorizedAccessException("This request is not an authentic webhook request.");
            }

            Request.InputStream.Position = 0;
            string data = new StreamReader(Request.InputStream).ReadToEnd().ToString().Trim();

            if (data == "")
            {
                Trace.TraceError("In CustErase WHK - No data inputstream");
                return "CustErase - Ok but no data inputstream";
            }

            string shop = (string)requestHeaders["X-Shopify-Shop-Domain"];
            string shopToken = GetShopToken(shop);

            // do a lot of work around clearing metafields when deleting cust

            Trace.TraceInformation("Leaving CustErase webhook ");
            return "CustErase - Ok";
        }

        // required for GDPR compliance - set from the Shopify partner app config menu
        public async Task<string> CustData()
        {
            Trace.TraceInformation("Starting Cust Data Request webhook ");
            var requestHeaders = Request.Headers.Keys.Cast<string>().ToDictionary(k => k, v => (StringValues)Request.Headers[v]);
            Stream inputStream = Request.InputStream;

            if (!await AuthorizationService.IsAuthenticWebhook(requestHeaders, Request.InputStream, secretKey))
            {
                throw new UnauthorizedAccessException("This request is not an authentic webhook request.");
            }

            Request.InputStream.Position = 0;
            string data = new StreamReader(Request.InputStream).ReadToEnd().ToString().Trim();

            if (data == "")
            {
                Trace.TraceError("In CustDataReq WHK - No data inputstream");
                return "Cust Data Req - Ok but no data inputstream";
            }

            string shop = (string)requestHeaders["X-Shopify-Shop-Domain"];
            string shopToken = GetShopToken(shop);

            // do a lot of work around clearing metafields when deleting cust

            Trace.TraceInformation("Leaving Cust Data Request webhook ");
            return "CustDataReq - Ok";
        }

        // required for GDPR compliance - set from the Shopify partner app config menu
        public async Task<string> ShopErase()
        {
            Trace.TraceInformation("Starting ShopErase webhook ");
            var requestHeaders = Request.Headers.Keys.Cast<string>().ToDictionary(k => k, v => (StringValues)Request.Headers[v]);
            Stream inputStream = Request.InputStream;

            if (!await AuthorizationService.IsAuthenticWebhook(requestHeaders, Request.InputStream, secretKey))
            {
                throw new UnauthorizedAccessException("This request is not an authentic webhook request.");
            }

            Request.InputStream.Position = 0;
            string data = new StreamReader(Request.InputStream).ReadToEnd().ToString().Trim();

            if (data == "")
            {
                Trace.TraceError("In ShopErase WHK - No data inputstream");
                return "ShopErase - Ok but no data inputstream";
            }

            string shop = (string)requestHeaders["X-Shopify-Shop-Domain"];
            string shopToken = GetShopToken(shop);

            // do a lot of work around clearing metafields when deleting shop

            Trace.TraceInformation("Leaving ShopErase webhook ");
            return "ShopErase - Ok";
        }

        public async Task<string> Products(string ProdType)
        {
            Trace.TraceInformation("Starting Products webhook " + ProdType);
            var requestHeaders = Request.Headers.Keys.Cast<string>().ToDictionary(k => k, v => (StringValues)Request.Headers[v]);
            Stream inputStream = Request.InputStream;

            if (!await AuthorizationService.IsAuthenticWebhook(requestHeaders, Request.InputStream, secretKey))
            {
                throw new UnauthorizedAccessException("This request is not an authentic webhook request.");
            }

            Request.InputStream.Position = 0;
            string data = new StreamReader(Request.InputStream).ReadToEnd().ToString().Trim();

            if (data == "")
            {
                Trace.TraceError("Products WHK - No data inputstream");
                return "Products - Ok but no data inputstream";
            }

            string shop = (string)requestHeaders["X-Shopify-Shop-Domain"];
            string shopToken = GetShopToken(shop);

            //do something with data
            var jsonSerializer = new JavaScriptSerializer();
            var prodData = JsonConvert.DeserializeObject<dynamic>(data);

            var variants = prodData.GetValue("variants");
            var title = (string)prodData["title"];

            Trace.TraceInformation("Products WHK for " + title );

            bool hasInvent = false;
            bool InventoryMgd = true;
            foreach (var v in variants)
            {
                var im = (string)v["inventory_management"];
                var iq = (string)v["inventory_quantity"];

                if (im != "shopify")
                {
                    InventoryMgd = false;
                    break;
                }

                if (im == "shopify" && Int32.Parse(iq) > 0)
                {
                    hasInvent = true;                   
                    break;
                }
            }


            if (!InventoryMgd)
            {
                Trace.TraceInformation((string)("Products WHK " + title + " not inventory managed"));
                return "Products Whk - Ok"; ;
            }



            var prodId = (string)prodData["id"];

            if (prodId == null)
            {

                Trace.TraceInformation((string)("Products WHK " + title + " has no id"));
                return "Products Whk - Ok"; ;
            }

            // Int64 pI = Int64.Parse(prodId);

            var pService = new ProductService(shop, shopToken);
            Product product;
            try
            {
                product = await pService.GetAsync(Int64.Parse(prodId));
            }
            catch (Exception e)
            {
                string st = "Products WHK  " + title + " - Error getting product from Shopify " + prodId + " " + e.ToString();
                Trace.TraceError(st);
                throw e;
            }

            if ( (hasInvent && product.PublishedAt != null)  || (!hasInvent && product.PublishedAt == null))
            {
                Trace.TraceInformation((string)("Products WHK " + title + "no change"));
                return "Products Whk - Ok"; ;
            }

            DateTimeOffset? pAt = null;

            if (hasInvent)
            {
                pAt = DateTime.UtcNow;
            }

            product = await pService.UpdateAsync(Int64.Parse(prodId), new Product()
            {
                PublishedAt = pAt
            });
            
            if (pAt == null)
                Trace.TraceInformation("Products WHK - set" + title + " to " + "POS only");
            else
                Trace.TraceInformation("Products WHK - set" + title + " to " + "Online and POS");

            return "Products Whk - Ok"; ;

        }


        public async Task<string> CartCreate()
        {
            Trace.TraceInformation("Starting CartsCreate webhook ");
            var requestHeaders = Request.Headers.Keys.Cast<string>().ToDictionary(k => k, v => (StringValues)Request.Headers[v]);
            Stream inputStream = Request.InputStream;

            if (!await AuthorizationService.IsAuthenticWebhook(requestHeaders, Request.InputStream, secretKey))
            {
                throw new UnauthorizedAccessException("This request is not an authentic webhook request.");
            }

            Request.InputStream.Position = 0;
            string data = new StreamReader(Request.InputStream).ReadToEnd().ToString().Trim();

            if (data == "")
            {
                Trace.TraceError("In CustCreate WHK - No data inputstream");
                return "ShopErase - Ok but no data inputstream";
            }

            string shop = (string)requestHeaders["X-Shopify-Shop-Domain"];
            string shopToken = GetShopToken(shop);

            // do a lot of work around clearing metafields when deleting shop

            Trace.TraceInformation("Leaving CartsCreate webhook ");
            return "CartCreates - Ok";
        }


        public async Task<string> Checkouts(string ChkTyp)
        {

            Trace.TraceInformation("Starting Checkouts webhook " + ChkTyp);
            var requestHeaders = Request.Headers.Keys.Cast<string>().ToDictionary(k => k, v => (StringValues)Request.Headers[v]);
            Stream inputStream = Request.InputStream;

            if (!await AuthorizationService.IsAuthenticWebhook(requestHeaders, Request.InputStream, secretKey))
            {
                throw new UnauthorizedAccessException("This request is not an authentic webhook request.");
            }

            Request.InputStream.Position = 0;
            string data = new StreamReader(Request.InputStream).ReadToEnd().ToString().Trim();

            if (data == "")
            {
                Trace.TraceError("Checkouts WHK - No data inputstream");
                return "Checkouts - Ok but no data inputstream";
            }

            string shop = (string)requestHeaders["X-Shopify-Shop-Domain"];
            string shopToken = GetShopToken(shop);

            //do something with data
            var jsonSerializer = new JavaScriptSerializer();
            var checkOutData = JsonConvert.DeserializeObject<dynamic>(data);
            var lineItems2 = checkOutData.GetValue("line_items");
            var cust = checkOutData.GetValue("customer");
            var lineItems = checkOutData.line_items;

            Trace.TraceInformation("CheckOuts WHK - Read inputstream");

            string email = "";

            if (cust == null)
            {
                Trace.TraceError("Checkouts WHK - No customer details associated with order - cant check for ESC$");
                return "Checkouts - Ok";
            }
            else
            { 
                email = (string)cust["email"];
                Trace.TraceInformation("CheckOuts WHK for " + email);
            }

            if (float.Parse((string)checkOutData["total_discounts"]) > 0)
            {
                foreach(var discount in checkOutData.discount_codes)
                {
                    if (((string)discount["code"]).Contains("ESC$"))
                    {
                        string str = "Checkouts WHK " + email + " - ESC$ discount already applied " + (string)discount["code"] as string;
                        Trace.TraceError(str);
                        return "Checkouts - Ok";
                    }
                }
            }

            var custId = (string)cust["id"];

            if (!String.IsNullOrEmpty(custId))
            {
                //get the customer
                var custService = new CustomerService(shop, shopToken);
                Customer customer;
                try
                {
                    customer = await custService.GetAsync(Int64.Parse(custId));
                }
                catch (Exception e)
                {
                    Trace.TraceError("CheckOuts WHK - Error getting customer from Shopify " + custId + " " + email + " "  + e.ToString());
                    throw e;
                }
                

                var metaService = new MetaFieldService(shop, shopToken);
                IEnumerable<MetaField> metafields;

                try
                {
                    metafields = await metaService.ListAsync(Int64.Parse(custId), "customers");
                }
                catch (Exception e)
                {
                    Trace.TraceError("CheckOuts WHK - Error getting metafields from Shopify " + custId + " " + email + " " + e.ToString());
                    throw e;
                }
                
                var escCards = _EscCardManager.MetatoEscList(metafields, noexpired: true);

                //get value of un-expired cards 
                var cardValue = 0;
                foreach (var card in escCards)
                {
                    cardValue = cardValue + Int32.Parse(card.value);
                }

                //get value of cart
                float escCartTotal = 0;
                var prodService = new ProductService(shop, shopToken);
                Product prod = new Product();
                
                foreach (var li in lineItems)
                {
                    if (li["product_id"] != null)
                    {
                        try
                        {
                            prod = await prodService.GetAsync(Int64.Parse((string)li["product_id"]));
                        }
                        catch (Exception e)
                        {
                            Trace.TraceError("CheckOuts WHK for " + email + "- Error getting product from Shopify " + (string)li["title"] + " " + e.ToString());
                            throw e;
                        }



                        if (!prod.Tags.Contains("na_E$"))
                        {
                            escCartTotal = escCartTotal + (float.Parse((string)li["price"]) * Int32.Parse((string)li["quantity"]));
                        }
                    }
                    else
                    {
                        Trace.TraceError("CheckOuts WHK for " + email + " Line item for " + (string)li["title"] + "has no product Id - perhaps manually entered");
                    }
                }

                float escDis = (float)cardValue / 100; // esc card values stored as cents
                if (escCartTotal < escDis)
                {
                    escDis = escCartTotal;
                }

                double escTag = 0;
                if (escDis < 100)
                    escTag = Math.Floor(escDis / 10) * 10;
                else if (escDis < 300)
                    escTag = Math.Floor(escDis / 20) * 20;
                else if (escDis < 1000)
                    escTag = Math.Floor(escDis / 50) * 50;
                else if (escDis < 3000)
                    escTag = Math.Floor(escDis / 100) * 10;
                else
                    escTag = 3000;

                // remove any previous ESC$ left in tags if customer has checked in then gone back to cart, updated and checked out again
                Regex rgx = new Regex(@"ESC\$\d+,|,ESC\$\d+|ESC\$\d+");

                Trace.TraceInformation("CheckOuts WHK for " + email + ", tags are " + customer.Tags);  
                string tags = rgx.Replace(customer.Tags,"") + ",ESC$" + escTag.ToString("0");
                customer = await custService.UpdateAsync(Int64.Parse(custId), new Customer()
                {
                    Tags = tags
                });

                string str = "Checkouts WHK for " + email + " - added ESC$" + escTag.ToString("0") + " to " + customer.Email + " tags." as string;
                Trace.TraceInformation(str);
            }

            Trace.TraceInformation("Leaving Checkouts WHK - " + ChkTyp + " for " + email);
            return "Checkouts - Ok";
        }

        public async Task<string> OrdersPaid()
        {
            Trace.TraceInformation("Starting OrdersPaid Webhook");
            var requestHeaders = Request.Headers.Keys.Cast<string>().ToDictionary(k => k, v => (StringValues)Request.Headers[v]);
            Stream inputStream = Request.InputStream;

            if (!await AuthorizationService.IsAuthenticWebhook(requestHeaders, Request.InputStream, secretKey))
            {
                throw new UnauthorizedAccessException("OrdersPaid - This request is not an authentic webhook request.");
            }

            Trace.TraceInformation("OrdersPaid WHK URL valid - Getting Shop");
            string shop = (string) requestHeaders["X-Shopify-Shop-Domain"];
            string shopToken = GetShopToken(shop);

            Request.InputStream.Position = 0;
            string data = new StreamReader(Request.InputStream).ReadToEnd().ToString().Trim();

            if (data == "")
            {
                Trace.TraceError("OrdersPaid WHK - No data inputstream");
                return "OrdersPaid - Ok but no data inputstream";
            }

            //do something with data
            var jsonSerializer = new JavaScriptSerializer();
            var ordersData = JsonConvert.DeserializeObject<dynamic>(data);
            var cust = ordersData.GetValue("customer");

            Trace.TraceInformation("OrdersPaid WHK - Read inputstream");

            if (cust == null)
            {
                Trace.TraceError("OrdersPaid WHK - No customer details associated with order - cant check for ESC$");
                return "OrdersPaid - Ok";
            }

            var custId = (string)cust["id"];
            string email = (string)cust["email"]; 
            
            if (String.IsNullOrEmpty(custId))
            {
                Trace.TraceError("OrdersPaid WHK - No customer id associated with customer - cant check for ESC$");
                return "OrdersPaid - Ok";
            }

            //get the customer
            Trace.TraceInformation("OrdersPaid WHK - Getting Customer " + custId + " " + email);
            var custService = new CustomerService(shop, shopToken);
            Customer customer = new Customer();

            try
            {
                customer = await custService.GetAsync(Int64.Parse(custId));
            }
            catch (Exception e)
            {
                Trace.TraceError("OrdersPaid WHK - Error getting customer from Shopify " + custId + " " + email + " " + e.ToString());
                throw e;
            }

            Trace.TraceInformation("OrdersPaid WHK - " + email + " - checking if customer is an ambassador");
            if (!String.IsNullOrEmpty(Ambassador) && customer.Tags.Contains(Ambassador))
            {
                Trace.TraceInformation("OrdersPaid WHK - " + email + " - customer is an Ambassador. No ESC$s allocated");
                return "OrdersPaid - Ok";
            }

            Trace.TraceInformation("OrdersPaid WHK - " + email + " - checking if need to send account activation mail");
            if (customer.State != "enabled" & customer.State != "declined")
               if( !customer.Tags.Contains("SMail"))
                {
                    //send a customer invite email

                    string url = "";
                    try
                    {
                        url = await custService.GetAccountActivationUrl(Int64.Parse(custId));
                    }
                    catch (Exception e)
                    {
                        Trace.TraceError("OrdersPaid WHK - " + email + " - Error getting account activation Url " + custId + " " + e.ToString());
                        throw e;
                    }
                

               Trace.TraceInformation("OrdersPaid WHK - " + email + " - Submiting Sendgrid Send Activation email");

                _SendEmail.submitactivationemail(customer.FirstName, customer.Email, url);
                string t = customer.Tags + ", SMail" + DateTime.UtcNow.ToString("yyyy-MM-dd");
                customer = await custService.UpdateAsync(Int64.Parse(custId), new Customer()
                {
                    Tags = t
                });

                }

            Trace.TraceInformation("OrdersPaid WHK - " + email + " - Getting EscCards");
            var metaService = new MetaFieldService(shop, shopToken);
            IEnumerable<MetaField> metafields;

            try
            {
                metafields = await metaService.ListAsync((long)customer.Id, "customers");
            }
            catch (Exception e)
            {
                Trace.TraceError("OrdersPaid WHK - " + email + " - Error getting metafields from Shopify " + custId + " " + e.ToString());
                throw e;
            }

           
            var escCards = _EscCardManager.MetatoEscList(metafields, noexpired: true, gtrthan0: true);

            long cardValue = 0;
            bool gcMatch = false;
            foreach (var card in escCards)
            {
                cardValue = cardValue + Int64.Parse(card.value);
                if (card.id == (string)ordersData["id"]) gcMatch = true;
            }

            if (gcMatch)
            {
                // already processed somewhere else so no work to do
                Trace.TraceInformation("OrdersPaid WHK - " + email + " - card exists for customer already. ");
                return "OrdersPaid - Ok";
            }

            float cDiscount = 0;
            
            if (float.Parse((string)ordersData["total_discounts"]) > 0)
            {
                foreach (var discount in ordersData.discount_codes)
                {
                    if (((string)discount["code"]).Contains("ESC$"))
                    {
                        cDiscount = float.Parse((string)discount["amount"]) * 100;
                        break;
                    }
                }
             }

             if (cDiscount > 0)
             {
                Trace.TraceInformation("OrdersPaid WHK - " + email + " - Revaluing ESCCards");
                foreach (var card in escCards)
                 {
                     float cAmnt = float.Parse(card.value);
                     if (cAmnt > cDiscount)
                     {
                         cAmnt = cAmnt - cDiscount;
                         cDiscount = 0;
                     }
                     else
                     {
                         cDiscount = cDiscount - cAmnt;
                         cAmnt = 0;
                     }

                     // okay now update the giftcard, but add to the usage log first
                     EscCardUsageLog uLog = new EscCardUsageLog();
                     uLog.udate = DateTime.UtcNow.ToString("dd-MM-yyyy");
                     uLog.amount = (Int32.Parse(card.value) - cAmnt).ToString("0");
                     uLog.order_id = ordersData.id;

                     if (card.usage == null)
                     {
                        card.usage = new List<EscCardUsageLog>();
                        card.usage.Add(uLog);
                    } else
                     {
                        card.usage.Add(uLog);
                     }
                     card.value = cAmnt.ToString("0");

                     //find the metafield that corresponds to the card
                     MetaField mMeta = new MetaField();
                     foreach (var m in metafields)
                     {
                         if (m.Key == "Giftcard-" + card.id)
                         {
                            mMeta = m;
                            break;
                         }
                     }

                     mMeta.Value = JsonConvert.SerializeObject(card);

                    try
                    {
                        mMeta = await metaService.UpdateAsync((long)mMeta.Id, mMeta);
                    }
                    catch (Exception e)
                    {
                        Trace.TraceError("OrdersPaid WHK - " + email + " - Error updatinfg Metafield into Shopify " + custId + " " + mMeta.Value + " " + e.ToString());
                        throw e;
                    }
                     

                     if (cDiscount == 0) break;

                 }

             }

            // check if address list contains a shop address, i.e. used click to collect, and remove them
            Trace.TraceInformation("OrdersPaid WHK - " + email + " - Cleanup Addresses");
            bool foundCompanyAddress = false;
             foreach (var address in customer.Addresses)
             {
                 if (!String.IsNullOrEmpty(address.Company))
                 {
                     if ("2Escape - KL|Dealer A|Dealer B|Dealer C|Dealer D".Contains(address.Company))
                     {
                         foundCompanyAddress = true;
                         break;
                     }
                 }
             }

             List<Address> addresses = new List<Address>();
             Address defAddress = new Address();
             if (foundCompanyAddress)
             {
                defAddress = customer.Addresses.First();
                bool fIndex = true;
                foreach (var address in customer.Addresses)
                {
                    if (fIndex)
                    {
                        addresses.Add(address);
                        addresses[0].Default = true;
                        fIndex = false;
                    }
                    else
                    {
                        if (!"2Escape - KL|Dealer A|Dealer B|Dealer C|Dealer D".Contains(address.Company)) addresses.Add(address);
                    }
                }
             }
             else
             {
                defAddress = customer.DefaultAddress;
                addresses = customer.Addresses.ToList();
             }

             // remove any ESC$ tags 
             Regex rgx = new Regex(@"ESC\$\d+,|,ESC\$\d+|ESC\$\d+");
             string tags = rgx.Replace(customer.Tags, "");

            Trace.TraceInformation("OrdersPaid WHK - " + email + " - Updating Customer addresses and tags");

            try
            {
                customer = await custService.UpdateAsync(Int64.Parse(custId), new Customer()
                {
                    DefaultAddress = defAddress,
                    Addresses = addresses,
                    Tags = tags
                });
            }
            catch (Exception e)
            {
                Trace.TraceError("OrdersPaid WHK - " + email + " - Error updating Customer addresses, tags etc " + custId + " " + e.ToString());
                throw e;
            }
            

            //see if customer has earned some esc$ and needs a new ESC$ card

            //get value of new ESC$ from line items
            Trace.TraceInformation("OrdersPaid WHK - " + email + " - see if customer has earned ESC$");
            float escRedeem = 0;
             var prodService = new ProductService(shop, shopToken);
             Regex regex = new Regex(@"(Esc\$_\d+)");
            Product prod;

             foreach (var li in ordersData.line_items)
             {

                Trace.TraceInformation("OrdersPaid WHK - " + email + " - getting line item details for " + (string)li["title"]);

                if (li["product_id"] != null)
                {
                        
                    try
                            {
                                prod = await prodService.GetAsync(Int64.Parse((string)li["product_id"]));
                            }
                            catch (Exception e) 
                            {
                                Trace.TraceError("OrdersPaid WHK - " + email + "- Error from Shopify getting line item product" + custId + " " + e.ToString());
                                throw e;
                            }
                    if (prod != null)
                    {
                        Match match = regex.Match(prod.Tags);
                        if (match.Success)
                        {

                            string esc = match.Value.Replace("Esc$_", "");
                            escRedeem = escRedeem + (float.Parse(esc) * Int32.Parse((string)li["quantity"]));

                        }
                    }
                }
                else
                {
                    Trace.TraceError("OrdersPaid WHK - " + email + " Line Item with title " + (string)li["title"] + " has a null product id. Perhaps manually entered");
                }
             }

             Trace.TraceInformation("OrdersPaid WHK - " + email + " - escRedeem is " + escRedeem.ToString("0"));

             if (escRedeem > 0)
             {

                var  cTitle = (string)ordersData["order_number"];

 //var cTitle = "";
//               if (String.IsNullOrEmpty(cust.firstname) || String.IsNullOrEmpty(cust.lastname))
 //                   cTitle = cust.email;
 //               else
 //                   cTitle = cust.firstname + " " + cust.lastname;

                var eDate = DateTime.UtcNow.AddDays(expiryPeriod).ToString("dd-MM-yyyy");


                var meta = _EscCardManager.EscCardtoMeta((string)ordersData["id"], cTitle, (escRedeem/100).ToString("0"), eDate, "Giftcard raised for value of " + (escRedeem/100).ToString("0.00"));
                try
                {
                    meta = await metaService.CreateAsync(meta, Int64.Parse(custId), "customers");
                }
                catch (Exception e)
                {
                    Trace.TraceError("OrdersPaid WHK - " + email + " - Error creating new meta " + custId + " " +  meta.Value + " " + e.ToString());
                    throw e;
                }

                Trace.TraceInformation("OrdersPaid WHK - " + email + " - created new Esc Card for " + escRedeem.ToString("0"));

            }

             
             Trace.TraceInformation("Completed Orders Paid webhook for " + email);
             return "OrderPaid - OK";

        }

        // GET: WebHooks
        public ActionResult Index()
        {
            return View();
        }
    }
}