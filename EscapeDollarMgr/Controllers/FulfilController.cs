﻿using System;
using System.Web.Mvc;
using System.Collections.Generic;
using System.Linq;
using System.Collections.Specialized;
using System.Security.Cryptography;
using EscapeDollarMgr.Tools;
using System.Text;
using EscapeDollarMgr.Data;
using ShopifySharp;
using System.Threading.Tasks;
using Microsoft.Extensions.Primitives;
using System.Diagnostics;


namespace EscapeDollarMgr.Controllers
{
    public class FulfilController : Controller
    {

        private string apiKey = AppSettings.ShopifyApiKey;
        private string secretKey = AppSettings.ShopifySecretKey;
        private string appUrl = AppSettings.ShopifyAppUrl;
        private string redirectUrl = AppSettings.redirectUrl;

        public ActionResult Login()
        {
            Trace.TraceInformation("Login, presenting view");
            return View();
        }

        // GET: fulfil
        public async Task<ActionResult> Install(string shop, string timestamp)
        {
            Trace.TraceInformation("Starting Install " + shop);
            //check if its a valid url
     
            bool isValidUrl = await AuthorizationService.IsValidShopDomainAsync(shop);

       //     bool isValidUrl = true;

            if (!isValidUrl)
            {
                Trace.TraceInformation("In Install - not a valid URL");
                ViewBag.Message = "Not a valid Shopify URL";
                return RedirectToAction("Error", "Home", "Not a valid URL");
            }
            else
            {
                //ok lets install shop
                Trace.TraceInformation("In Install - valid URL");
                var scopes = new List<string>()
                    {
                     "read_products",
                     "write_products",
                     "read_customers",
                     "write_customers",
                     "read_orders",
                     "write_orders",
                     "read_shipping",
                     "write_shipping"
                    };

                string authUrl = AuthorizationService.BuildAuthorizationUrl(scopes, shop, apiKey, redirectUrl).ToString();
                // string authUrl = string.Format("https://{0}/admin/oauth/authorize?client_id={1}&scope=read_customers&redirect_uri=https://{2}/fulfil/auth", shop, apiKey, appUrl);
                Trace.TraceInformation("Leaving Install");
                return Redirect(authUrl);
            }      
                       
        }

        public async Task<ActionResult> Auth(string shop, string code)
        {
            Trace.TraceInformation("Starting Auth " + shop);
            var qs = Request.QueryString.Keys.Cast<string>().ToDictionary(k => k, v => (StringValues)Request.QueryString[v]);
            

            if (AuthorizationService.IsAuthenticRequest(qs, secretKey))
            {
                Trace.TraceInformation("In Auth - is valid auth url. Getting access token " + code + " shop " + shop + " apiKey "  + apiKey + " secret " + secretKey);
                string accessToken = await AuthorizationService.Authorize(code, shop, apiKey, secretKey);
                Trace.TraceInformation("In Auth - retrieved access token ");
                Session["Shop"] = shop;
                Session["ShopToken"] = accessToken;
                Trace.TraceInformation("In Auth - Saving shop token ");
                SaveShopToken(shop, (string)accessToken);

                Trace.TraceInformation("In Auth - Creating webhooks ");
                //install webhooks associated with app
                await CreateWebhooks(shop, accessToken);

                await CreateShippingService(shop, accessToken);

                Trace.TraceInformation("Leaving Auth - successfully installed " + shop);
                return RedirectToAction("Index", "Home");
            } else
            {
                Trace.TraceInformation("In Auth - not a valid auth url for " + shop);
                return RedirectToAction("Error", "Home", "Not a valid Shopify Authorization requst");
            }

           // return RedirectToAction("Error", "Home", "Not a valid Shopify Authorization requst - something went wrong");
        }

        private async Task CreateShippingService(string shop, string token)
        {
            Trace.TraceInformation("Starting CreateShippingService " + shop);
            var service = new CarrierService(shop, token);

            IEnumerable<Carrier> carrierList = await service.ListAsync();
            var foundC = false;

            foreach (var c in carrierList)
            {
                if (c.Name == "2Escape")
                {
                    foundC = true;
                    break;
                }
            }


            if (foundC)
            {
                Trace.TraceInformation("Leaving CreateShippingService - found service" + shop);
                return;
            }

            Carrier carrier = new Carrier
            {
                Active = true,
                CallbackUrl = "https://" + appUrl + "/home/Shipping",
                CarrierServiceType = "api",
                Name = "2Escape",
                ServiceDiscovery = false,
                Format = "json"
            };

            var carrierService = await service.CreateAsync(carrier);

            Trace.TraceInformation("Leaving CreateShippingService" + shop);
            return;

        }

            private async Task CreateWebhooks(string shop, string token)
        {
            Trace.TraceInformation("Starting CreateWebHooks " + shop);
            var service = new WebhookService(shop, token);

            IEnumerable<Webhook> webhooks = await service.ListAsync();

            bool foundCC = false, foundCU = false, foundOP = false, foundCE = false, foundSE = false;
            bool foundCartsC = false, foundPC = false, foundPU = false;
            bool foundCD = true;

            foreach (var wH in webhooks)
            {
                if (wH.Topic == "checkouts/create") foundCC = true;
                else if (wH.Topic == "checkouts/update") foundCU = true;
                else if (wH.Topic == "orders/paid") foundOP = true;
                else if (wH.Topic == "carts/create") foundCartsC = true;
                else if (wH.Topic == "customers/redact") foundCE = true;
                else if (wH.Topic == "customers/data_request") foundCD = true;
                else if (wH.Topic == "shop/redact") foundSE = true;
                else if (wH.Topic == "products/create") foundPC = true;
                else if (wH.Topic == "products/update") foundPU = true;
            }

     //       foundCC = false;
            if (!foundCC)
            {
                var hook = new Webhook()
                {
                    Address = "https://" + appUrl + "/webhooks/checkouts?ChkTyp=Create",
                    CreatedAt = DateTime.UtcNow,
                    Format = "json",
                    Topic = "checkouts/create"
                };

                try
                {
                    hook = await service.CreateAsync(hook);
                }
                catch (ShopifyException e)
                {
                    Trace.TraceError("Error creating checkouts create Webhook" + e.ToString());
                    throw e;
                }
            }

    //        foundCU = false;
            if (!foundCU)
            {
                var hook = new Webhook()
                {
                    Address = "https://" + appUrl + "/webhooks/checkouts?ChkTyp=Update",
                    CreatedAt = DateTime.UtcNow,
                    Format = "json",
                    Topic = "checkouts/update"
                };

                try
                {
                    hook = await service.CreateAsync(hook);
                }
                catch (ShopifyException e)
                {
                    Trace.TraceError("Error creating Checkouts update Webhook" + e.ToString());
                    throw e;
                }
            }

  //          foundOP = true;
            if (!foundOP)
            {
                var hook = new Webhook()
                {
                    Address = "https://" + appUrl + "/webhooks/orderspaid",
                    CreatedAt = DateTime.UtcNow,
                    Format = "json",
                    Topic = "orders/paid"
                };

                try
                {
                    hook = await service.CreateAsync(hook);
                }
                catch (ShopifyException e)
                {
                    Trace.TraceError("Error creating OrdersPaid Webhook" + e.ToString());
                    throw e;
                }
            }

            // at the moment can only confiure these from the application partner app set window
            foundCE = true;
            if (!foundCE)
            {
                var hook = new Webhook()
                {
                    Address = "https://" + appUrl + "/webhooks/custerase",
                    CreatedAt = DateTime.UtcNow,
                    Format = "json",
                    Topic = "customers/redact"
                };

                try
                {
                    hook = await service.CreateAsync(hook);
                }
                catch (ShopifyException e)
                {
                    Trace.TraceError("Error creating  custerase Webhook" + e.ToString());
                    throw e;
                }
            }

            // at the moment can only confiure these from the application partner app set window
            foundSE = true;
            if (!foundSE)
            {
                var hook = new Webhook()
                {
                    Address = "https://" + appUrl + "/webhooks/shoperase",
                    CreatedAt = DateTime.UtcNow,
                    Format = "json",
                    Topic = "shop/redact"
                };

                try
                {
                    hook = await service.CreateAsync(hook);
                }
                catch (ShopifyException e)
                {
                    Trace.TraceError("Error creating shoperase Webhook" + e.ToString());
                    throw e;
                }
            }

            // at the moment can only confiure these from the application partner app set window
            foundCD = true;
            if (!foundCD)
            {
                var hook = new Webhook()
                {
                    Address = "https://" + appUrl + "/webhooks/custdata",
                    CreatedAt = DateTime.UtcNow,
                    Format = "json",
                    Topic = "customers/data_request"
                };

                try
                {
                    hook = await service.CreateAsync(hook);
                }
                catch (ShopifyException e)
                {
                    Trace.TraceError("Error creating customer datarequest Webhook" + e.ToString());
                    throw e;
                }
            }




            foundCartsC = true;
            if (!foundCartsC)
            {
                var hook = new Webhook()
                {
                    Address = "https://" + appUrl + "/webhooks/cartcreate",
                    CreatedAt = DateTime.UtcNow,
                    Format = "json",
                    Topic = "carts/update"
                };

                try
                {
                    hook = await service.CreateAsync(hook);
                }
                catch (ShopifyException e)
                {
                    Trace.TraceError("Error creating cartscreate Webhook" + e.ToString());
                    throw e;
                }
            }

            if (!foundPU)
            {
                var hook = new Webhook()
                {
                    Address = "https://" + appUrl + "/webhooks/products?ProdType=Update",
                    CreatedAt = DateTime.UtcNow,
                    Format = "json",
                    Topic = "products/update"
                };

                try
                {
                    hook = await service.CreateAsync(hook);
                }
                catch (ShopifyException e)
                {
                    Trace.TraceError("Error creating products update Webhook" + e.ToString());
                    throw e;
                }
            }

            if (!foundPC)
            {
                var hook = new Webhook()
                {
                    Address = "https://" + appUrl + "/webhooks/products?ProdType=Create",
                    CreatedAt = DateTime.UtcNow,
                    Format = "json",
                    Topic = "products/create"
                };

                try
                {
                    hook = await service.CreateAsync(hook);
                }
                catch (ShopifyException e)
                {
                    Trace.TraceError("Error creating products create Webhook" + e.ToString());
                    throw e;
                }
            }




            Trace.TraceInformation("Leaving CreateWebHooks " + shop);
            return;
        }
        private void SaveShopToken(string shop, string token)
        {
            Trace.TraceInformation("In Fulful SaveShopToken " + shop);
            using (var con = new EscapeDollarMgrEntities())
            {
                //if this is a new shop
                if(con.ShopDatas.Any(c=>c.Shop == shop))
                {
                    ShopData x = con.ShopDatas.First(c => c.Shop == shop);
                    x.Token = token;
                    x.InstallDate = DateTime.UtcNow;
                }
                else
                {
                    //new shop
                    con.ShopDatas.Add(new ShopData { Shop = shop, Token = token, InstallDate = DateTime.UtcNow });

                }
                con.SaveChanges();
            }
        }

        //this function no longer used as now use the ShopifySharp one
        public static bool IsAuthenticRequest(NameValueCollection querystring, string shopifySecretKey)
        {
            string hmac = querystring.Get("hmac");

            if (string.IsNullOrEmpty(hmac))
            {
                return false;
            }

            Func<string, bool, string> replaceChars = (string s, bool isKey) =>
            {
                //Important: Replace % before replacing &. Else second replace will replace those %25s.
                string output = (s?.Replace("%", "%25").Replace("&", "%26")) ?? "";

                if (isKey)
                {
                    output = output.Replace("=", "%3D");
                }

                return output;
            };

            var kvps = querystring.Cast<string>()
                .Select(s => new { Key = replaceChars(s, true), Value = replaceChars(querystring[s], false) })
                .Where(kvp => kvp.Key != "signature" && kvp.Key != "hmac")
                .OrderBy(kvp => kvp.Key)
                .Select(kvp => $"{kvp.Key}={kvp.Value}");

            var hmacHasher = new HMACSHA256(Encoding.UTF8.GetBytes(shopifySecretKey));
            var hash = hmacHasher.ComputeHash(Encoding.UTF8.GetBytes(string.Join("&", kvps)));

            //Convert bytes back to string, replacing dashes, to get the final signature.
            var calculatedSignature = BitConverter.ToString(hash).Replace("-", "");

            //Request is valid if the calculated signature matches the signature from the querystring.
            return calculatedSignature.ToUpper() == hmac.ToUpper();
        }
    }


}