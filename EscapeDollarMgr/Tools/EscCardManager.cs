﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Web;
using ShopifySharp;
using EscapeDollarMgr.Models;
using Newtonsoft.Json;
using System.Diagnostics;

namespace EscapeDollarMgr.Tools
{



    public class EscCardManager : IEscCardManager
    {
        private CultureInfo dtCulture = AppSettings.DateTimeCulture;

        public string OtherLoyDol(IEnumerable<MetaField> Metas)
        {


            foreach (var meta in Metas)
            {

                if (meta.Namespace == "LoyaltyOtherVendor" && meta.Key == "LoyaltyOtherVendor")
                {
                    return meta.Value.ToString();
                }
            }

            return "Not Set";
        }


        public IEnumerable<EscCardObj> MetatoEscList(IEnumerable<MetaField> Metas, bool noexpired = false, bool gtrthan0 = false)
        {
            List<EscCardObj> escCards = new List<EscCardObj>();

            var card = new EscCardObj();
            var dateNow = DateTime.UtcNow;

            Trace.TraceInformation("In MetatoEscList");

            foreach (var meta in Metas)
            {

                if (meta.Description.Contains("Loyalty Giftcard"))
                {
                    card = JsonConvert.DeserializeObject<EscCardObj>(meta.Value.ToString());
                    var cdate = DateTime.Parse(card.expiry_date, dtCulture);
                    if (noexpired & cdate < dateNow) continue;
                    if (gtrthan0 & Int64.Parse(card.value) <= 0) continue;
                    escCards.Add(card);
                }
            }

            Trace.TraceInformation("Leaving MetatoEscList");
            return escCards;

        }


        public MetaField EscCardtoMeta(string cardId, string title, string value, string date, string auditNote)
        {
            var escCard = new EscCardObj();
            var metaCard = new MetaField();
            var auditLog = new EscCardAuditLog();

            Trace.TraceInformation("In EscCardtoMeta");

            escCard.id = cardId;
            escCard.title = title;
            escCard.value = (float.Parse(value) * 100).ToString();
            escCard.created_at = DateTime.UtcNow.ToString("dd-MM-yyyy");
            escCard.expiry_date = date;
            auditLog.adate = escCard.created_at;
            auditLog.anote = auditNote;
            escCard.audit_log = new List<EscCardAuditLog>
            {
                auditLog
            };
            metaCard.Key = "Giftcard-" + cardId;
            metaCard.Description = "Loyalty Giftcard-" + cardId;
            metaCard.Namespace = "Loyalty";
            metaCard.ValueType = "string";
            metaCard.Value = JsonConvert.SerializeObject(escCard);

            Trace.TraceInformation("Leaving EscCardtoMeta");
            return metaCard;
        }

        public MetaField UpdateMeta(MetaField meta, string value, string expiryDate, string note = "")
        {
            Trace.TraceInformation("In UpdateMeta");
            var card = JsonConvert.DeserializeObject<EscCardObj>(meta.Value.ToString());
            var auditLog = new EscCardAuditLog();

            auditLog.adate = DateTime.UtcNow.ToString("dd-MM-yyyy");
            if (!string.Equals(card.value, (float.Parse(value) * 100).ToString("F2") ))
            {
                auditLog.anote = "Updated value from " + (float.Parse(card.value) / 100).ToString() + " to " + (float.Parse(value)).ToString() + ".";
                card.value = (float.Parse(value) * 100).ToString();
            }

            if (!string.Equals(card.expiry_date, expiryDate))
            {
                auditLog.anote = "Updated expiry date from " + card.expiry_date + " to " + expiryDate + ".";
                card.expiry_date = expiryDate;
            }
            auditLog.anote = auditLog.anote + " " + note;
            card.audit_log.Add(auditLog);

            meta.Value = JsonConvert.SerializeObject(card);

            Trace.TraceInformation("Leaving UpdateMeta");
            return meta;
        }

    }
}