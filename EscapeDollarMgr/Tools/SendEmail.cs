﻿using System;
using System.Net.Mail;
using System.Net.Mime;
using System.Diagnostics;
using System.Web.Script.Serialization;
using Microsoft.WindowsAzure.Storage; // Namespace for CloudStorageAccount
using Microsoft.WindowsAzure.Storage.Queue; // Namespace for Queue storage types

// not using this at the moment because using sendgrid to send email
namespace EscapeDollarMgr.Tools
{

    public class MailUrlModel
    {
        public string firstName;
        public string email;
        public string aUrl;
    }

    public class SendEmail
    {
    

        private string mHost = "smtp.office365.com";
        private int mPort = 587;
        private string mUserName = "sales@2escape.bike";
        private string mPassword = "!#Sal3s23scape+";
        private int mTimeout = 20000;

        private string _storageQueue = AppSettings.storageQueue;

        private SmtpClient setClient ()
        {
            SmtpClient client = new SmtpClient();
            client.Port = mPort;
            client.Host = mHost;
            client.Timeout = mTimeout;
            client.DeliveryMethod = SmtpDeliveryMethod.Network;
            client.UseDefaultCredentials = false;
            client.Credentials = new System.Net.NetworkCredential(mUserName, mPassword);
            client.EnableSsl = true;
            return client;
        }

        public void submitactivationemail(string firstName, string email, string aUrl)
        {
            // this puts a job on the sendaccountactivateurl queue under Azure escapedollarmgrstorage storage account
            MailUrlModel m = new MailUrlModel();
            if (String.IsNullOrEmpty(firstName))
                m.firstName = " ";
            else
                m.firstName = firstName;

            m.email = email;
            m.aUrl = aUrl;

            var serializer = new JavaScriptSerializer();
            var mstr = serializer.Serialize(m);

            // emailconfirmation queue
            var account = CloudStorageAccount.Parse(_storageQueue);
            var queueClient = account.CreateCloudQueueClient();
            var queue = queueClient.GetQueueReference("sendaccountactivateurl");
            queue.CreateIfNotExists();

            queue.AddMessage(new CloudQueueMessage(mstr));

        }

        // this no longer used. Now use webjob and sendgrid (see submitactivationemail above)
        public void SendAccountActivateEmail(string firstName, string email, string aUrl)

        {
            Trace.TraceInformation("Starting SendAccountActivateEmail");

            MailMessage objToMail = new MailMessage();
            SmtpClient client = setClient();

            objToMail.From = new MailAddress(mUserName);
            objToMail.To.Add(new MailAddress(email));
            objToMail.Subject = "Customer Account Activation";

            string plainTextBody = System.IO.File.ReadAllText(System.Web.HttpContext.Current.Server.MapPath(@"~/MailTemplates/SendAccountActivation.txt"));
            plainTextBody = plainTextBody.Replace(@"##fname##", firstName);
            plainTextBody = plainTextBody.Replace(@"##url##", aUrl);
            objToMail.Body = plainTextBody;

            string htmlBody = System.IO.File.ReadAllText(System.Web.HttpContext.Current.Server.MapPath(@"~/MailTemplates/SendAccountActivation.html"));

            htmlBody = htmlBody.Replace(@"##fname##", firstName);
            htmlBody = htmlBody.Replace(@"##url##", aUrl);
            AlternateView htmlView = AlternateView.CreateAlternateViewFromString(htmlBody, null, MediaTypeNames.Text.Html);

            objToMail.AlternateViews.Add(htmlView);

            try
            {
                client.Send(objToMail);
            }
            catch (Exception ex)
            {
                Trace.TraceInformation("SendAccountActivateEmail - send mail Exception" + ex.Message);
            }
        }


    }
}