﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using EscapeDollarMgr.Models;
using ShopifySharp;

namespace EscapeDollarMgr.Tools
{
    public interface IEscCardManager
    {
        IEnumerable<EscCardObj> MetatoEscList(IEnumerable<MetaField> Metas, bool noexpired = false, bool gtrthan0 = false);

        string OtherLoyDol(IEnumerable<MetaField> Metas);

    }
}
