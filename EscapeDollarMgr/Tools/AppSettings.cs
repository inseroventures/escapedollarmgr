﻿using System;
using System.Configuration;
using System.Globalization;

namespace EscapeDollarMgr.Tools
{
    public class AppSettings
    {

        private static string GetKey(string key)
        {
            string value = Environment.GetEnvironmentVariable("APPSETTING_" + key)
                            ?? Environment.GetEnvironmentVariable(key)
                            ?? ConfigurationManager.AppSettings[key]
                            ?? ConfigurationManager.AppSettings[key];
            return value;
        }

        
        public static string ShopifySecretKey { get; } = GetKey("Shopify_Secret_Key");
        public static string ShopifyApiKey { get; } = GetKey("Shopify_API_Key");
        public static string ShopifyAppUrl { get; } = GetKey("Shopify_App_Url");

        public static string ExpiryPeriod { get; } = GetKey("Expiry_Period");

        public static string redirectUrl { get; } = GetKey("reDirectUrl");

        public static string storageQueue { get; } = GetKey("StorageQueue");

        public static CultureInfo DateTimeCulture = CultureInfo.CreateSpecificCulture("en-GB");

        public static string Ambassador { get; } = GetKey("Ambassador");

    }
}