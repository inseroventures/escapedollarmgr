﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Newtonsoft.Json;

namespace EscapeDollarMgr.Models
{
    public class ShipRateAddress
    {
        /// <summary>
        /// Country.
        /// </summary>
        [JsonProperty("country")]
        public string Country { get; set; }

        /// <summary>
        /// Postal Code.
        /// </summary>
        [JsonProperty("postal_code")]
        public string PostalCode { get; set; }

    
        /// <summary>
        /// Province or State.
        /// </summary>
        [JsonProperty("province")]
        public string Province { get; set; }

        /// <summary>
        /// City.
        /// </summary>
        [JsonProperty("city")]
        public string City { get; set; }

        /// <summary>
        /// The Name of customer.
        /// </summary>
        [JsonProperty("name")]
        public string Name { get; set; }

        /// <summary>
        /// Address Line 1.
        /// </summary>
        [JsonProperty("address1")]
        public string Address1 { get; set; }

        /// <summary>
        /// Address Line 2.
        /// </summary>
        [JsonProperty("address2")]
        public string Address2 { get; set; }

        /// <summary>
        /// Address Line 3.
        /// </summary>
        [JsonProperty("address3")]
        public string Address3 { get; set; }

        /// <summary>
        /// The Phone number.
        /// </summary>
        [JsonProperty("phone")]
        public string Phone { get; set; }

        /// <summary>
        /// Fax number.
        /// </summary>
        [JsonProperty("fax")]
        public string Fax { get; set; }

        /// <summary>
        /// Address Type.
         /// </summary>
        [JsonProperty("address_type")]
        public string AddressType { get; set; }

        /// <summary>
        /// Company Name.
        /// </summary>
        [JsonProperty("company_name")]
        public string CompanyName { get; set; }
    }
}