﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Newtonsoft.Json;

namespace EscapeDollarMgr.Models
{
    public class ShipRateLineItem
    {
        /// <summary>
        /// The Name of the product.
        /// </summary>
        [JsonProperty("name")]
        public string Name { get; set; }

        /// <summary>
        /// The item unique identifier.
        /// </summary>
        [JsonProperty("sku")]
        public string Sku { get; set; }

        /// <summary>
        /// Quantity of the item.
        /// </summary>
        [JsonProperty("quantity")]
        public int Quantity { get; set; }

        /// <summary>
        /// Weight in grams
        /// </summary>
        [JsonProperty("grams")]
        public int Grams { get; set; }



        /// <summary>
        /// price.
        /// </summary>
        [JsonProperty("price")]
        public string Price { get; set; }

        /// <summary>
        /// The Brand of the item
        /// </summary>
        [JsonProperty("vendor")]
        public string Vendor { get; set; }

        /// <summary>
        /// Does it require shipping.
        /// </summary>
        [JsonProperty("requires_shipping")]
        public bool RequiresShipping { get; set; }

        /// <summary>
        /// Is it taxable.
        /// </summary>
        [JsonProperty("taxable")]
        public bool Taxable { get; set; }

        /// <summary>
        /// What type is the fulfilment service.
        /// </summary>
        [JsonProperty("fulfillment_service")]
        public string FulfilmentService { get; set; }
    }
}