﻿using System;
using System.Collections.Generic;


namespace EscapeDollarMgr.Models
{

    public partial class EscCardAuditLog
    {
        public string adate { get; set; }
        public string anote { get; set; }
       
    }

    public partial class EscCardUsageLog
    {
        public string udate { get; set; }
        public string amount { get; set; }

        public string order_id { get; set; }

    }

    public partial class EscCardObj
    {
        public string id { get; set; }
        public string title { get; set; }
        public string value { get; set; }
        public string created_at { get; set; }
        public string expiry_date { get; set; }
        public int expiry_period { get; set; }

        public List<EscCardAuditLog> audit_log {get; set;}

        public List<EscCardUsageLog> usage { get; set; }

    }
}