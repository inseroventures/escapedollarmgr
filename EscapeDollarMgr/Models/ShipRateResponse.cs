﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Newtonsoft.Json;

namespace EscapeDollarMgr.Models
{
    public class ShipRateResponse
    {
        /// <summary>
        /// A list of rates
        /// </summary>
        [JsonProperty("rates")]
        public IEnumerable<ShipRateResponseItem> Rates { get; set; }
    }

    public class ShipRateResponseItem
    {
        /// <summary>
        /// Service Name.
        /// </summary>
        [JsonProperty("service_name")]
        public string ServiceName { get; set; }

        /// <summary>
        /// Service Code.
        /// </summary>
        [JsonProperty("service_code")]
        public string ServiceCode { get; set; }

        /// <summary>
        /// Total Price.
        /// </summary>
        [JsonProperty("total_price")]
        public string TotalPrice { get; set; }

        /// <summary>
        /// Description.
        /// </summary>
        [JsonProperty("description")]
        public string Description { get; set; }

        /// <summary>
        /// Currency.
        /// </summary>
        [JsonProperty("currency")]
        public string Currency { get; set; }

        /// <summary>
        /// Minimum Delivery Date.- 2013-04-12 14:48:45 -0400
        /// </summary>
        [JsonProperty("min_delivery_date")]
        public string MinDeliveryDate { get; set; }

        /// <summary>
        /// Maximum Deliery Date 2013-04-12 14:48:45 -0400.
        /// </summary>
        [JsonProperty("max_delivery_date")]
        public string MaxDeliveryDate { get; set; }
    }
}