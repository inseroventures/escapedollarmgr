﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Newtonsoft.Json;

namespace EscapeDollarMgr.Models
{

    public class ShipRateRequest
    {
        /// <summary>
        /// Rate request.
        /// </summary>
        [JsonProperty("rate")]
        public ShipRateRequestItem rate { get; set; }
    }

    public class ShipRateRequestItem
    {
        /// <summary>
        /// Origin.
        /// </summary>
        [JsonProperty("origin")]
        public ShipRateAddress Origin { get; set; }


        /// <summary>
        /// Destination.
        /// </summary>
        [JsonProperty("destination")]
        public ShipRateAddress Destination { get; set; }


        /// <summary>
        /// Line Items.
        /// </summary>
        [JsonProperty("items")]
        public IEnumerable<ShipRateLineItem> Items { get; set; }

        /// <summary>
        /// Currency.
        /// </summary>
        [JsonProperty("currency")]
        public string Currency { get; set; }


    }
}