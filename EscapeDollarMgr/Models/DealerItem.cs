﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Newtonsoft.Json;

namespace EscapeDollarMgr.Models
{
    public class DealerItem
    { 

        /// <summary>
        /// Uniuque key
        /// </summary>
        [JsonProperty("key")]
         public string Key { get; set; }

        /// <summary>
        /// Name of Dealer.
        /// </summary>
        [JsonProperty("dealer")]
        public string Dealer { get; set; }

        /// <summary>
        /// Address Line 1.
        /// </summary>
        [JsonProperty("address1")]
        public string Address1 { get; set; }

        /// <summary>
        /// Address Line 2.
        /// </summary>
        [JsonProperty("address2")]
        public string Address2 { get; set; }

        /// <summary>
        /// city.
        /// </summary>
        [JsonProperty("city")]
        public string City { get; set; }

        /// <summary>
        /// Province or State.
        /// </summary>
        [JsonProperty("state")]
        public string State { get; set; }

        /// <summary>
        /// Country.
        /// </summary>
        [JsonProperty("country")]
        public string Country { get; set; }

        /// <summary>
        /// Postal Code.
        /// </summary>
        [JsonProperty("postal_code")]
        public string PostCode { get; set; }

        /// <summary>
        /// Postal Code.
        /// </summary>
        [JsonProperty("dealer_type")]
        public string DealerType { get; set; }

        /// <summary>
        /// Dealer Contact.
        /// </summary>
        [JsonProperty("contact")]
        public string Contact { get; set; }

        /// <summary>
        /// Phone Number.
        /// </summary>
        [JsonProperty("phone")]
        public string Phone { get; set; }

        /// <summary>
        /// Dealer can build Normal bikes.
        /// </summary>
        [JsonProperty("normal")]
        public bool Normal { get; set; }

        /// <summary>
        /// Dealer can build Triathlon bikes.
        /// </summary>
        [JsonProperty("triathlon")]
        public bool Triathlon { get; set; }

        /// <summary>
        /// Dealer can build HiRoad bikes.
        /// </summary>
        [JsonProperty("hiroad")]
        public bool HiRoad { get; set; }

        /// <summary>
        /// Dealer can build HiMTB bikes.
        /// </summary>
        [JsonProperty("himtb")]
        public bool HiMTB { get; set; }

        /// <summary>
        /// Opening Times.
        /// </summary>
        [JsonProperty("opening_times")]
        public IEnumerable<DealerOpeningTimes> OpeningTimes { get; set; }

    }

    public class DealerOpeningTimes
    {
        /// <summary>
        /// Day. 0 is Sunday
        /// </summary>
        [JsonProperty("day")]
        public int Day { get; set; }

        /// <summary>
        /// Open.
        /// </summary>
        [JsonProperty("o")]
        public string Open { get; set; }

        /// <summary>
        /// Close.
        /// </summary>
        [JsonProperty("c")]
        public string Close { get; set; }

        /// <summary>
        /// Is it open at all. True if not open
        /// </summary>
        [JsonProperty("d")]
        public bool NotOpen { get; set; }
    }
}