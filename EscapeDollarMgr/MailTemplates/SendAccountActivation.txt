﻿2Escape

Activate your account

Hi ##fname##, you've just made a purchase at 2Escape.  We have created you
a new customer account. All you have to do is activate it and choose a password.

  <a href="##url##" > Activate your account>

or

  <a href="https://2escape.bike/" > Vist the store </a>
